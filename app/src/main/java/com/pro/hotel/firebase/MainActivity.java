package com.pro.hotel.firebase;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.github.arturogutierrez.Badges;
import com.github.arturogutierrez.BadgesNotSupportedException;
import com.pro.hotel.R;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(Config.TOKEN, " Not Available");
        FirebaseMessaging.getInstance().subscribeToTopic(token);*/
    }

    public void TenCount (View view){
        try {
            Badges.setBadge(MainActivity.this, 10);
        } catch (BadgesNotSupportedException e) {
            Toast.makeText(this, "That was a Error!", Toast.LENGTH_SHORT).show();
        }
    }
    public void FiftyCount (View view){
        try {
            Badges.setBadge(MainActivity.this, 50);
        } catch (BadgesNotSupportedException e) {
            Toast.makeText(this, "That was a Error!", Toast.LENGTH_SHORT).show();
        }
    }
    public void ZeroCount (View view){
        try {
            Badges.setBadge(MainActivity.this, 0);
        } catch (BadgesNotSupportedException e) {
            Toast.makeText(this, "That was a Error!", Toast.LENGTH_SHORT).show();
        }
    }
}
