package com.pro.hotel.kilatiron;

import androidx.annotation.BinderThread;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.activity.LoginActivity;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.localcode.activity.SignUp;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.imageViewDate)
    ImageView imageViewDate;
    @BindView(R.id.tilUsername)
    TextInputLayout textInputLayoutUsername;
    @BindView(R.id.tilNoKTP)
    TextInputLayout textInputLayoutNoKTP;
    @BindView(R.id.tilJenisKelamin)
    TextInputLayout textInputLayoutJenisKelamin;
    @BindView(R.id.tilTempatLahir)
    TextInputLayout textInputLayoutTempatLahir;
    @BindView(R.id.tilTanggalLahir)
    TextInputLayout textInputLayoutTanggalLahir;
    @BindView(R.id.tilNoTel)
    TextInputLayout textInputLayoutNoTelephone;
    @BindView(R.id.tilAgama)
    TextInputLayout textInputLayoutAgama;
    @BindView(R.id.tilAlamatLengkap)
    TextInputLayout textInputLayoutAlamatLengkap;
    @BindView(R.id.tilEmail)
    TextInputLayout textInputLayoutEmail;
    @BindView(R.id.tilPassword)
    TextInputLayout textInputLayoutPassword;
    @BindView(R.id.tilRePassword)
    TextInputLayout textInputLayoutRePassword;
    @BindView(R.id.spinnerGender)
    Spinner spinnerGender;
    @BindView(R.id.spinnerReligion)
    Spinner spinnerReligion;
    @BindView(R.id.editTextUsername)
    EditText editTextNamaLengkap;
    @BindView(R.id.editTextNotel)
    EditText editTextNoHandphone;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.editTextRepassword)
    EditText editTextRePassword;
    @BindView(R.id.editTextAlamatLengkap)
    EditText editTextAlamat;
    @BindView(R.id.editTextTempatLahir)
    EditText editTextTempatLahir;
    @BindView(R.id.editTextTanggalLahir)
    EditText editTextTanggalLahir;
    @BindView(R.id.editTextNoKTP)
    EditText editTextNoKtp;
    @BindView(R.id.buttonSignUp)

    Button buttonSimpan;
    String mJenisKelamin, mTempatTanggaLahir;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat simpleDateFormat;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        String[] activity = new String[]{
                "Laki-laki",
                "Perempuan"
        };
        List<String> list = new ArrayList<>(Arrays.asList(activity));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, list
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapter);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mJenisKelamin = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        imageViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                datePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, dayOfMonth);
                        editTextTanggalLahir.setText(simpleDateFormat.format(newDate.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        mTempatTanggaLahir = editTextTempatLahir.getText().toString() + " " + editTextTanggalLahir.getText().toString();

        buttonSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue requestQueue = Volley.newRequestQueue(RegisterActivity.this);
                StringRequest stringRequest = new StringRequest(
                        Request.Method.POST,
                        Config.BASE_URL + Config.REGISTER,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if (validation()){
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        boolean success = jsonObject.getBoolean("success");
                                        String message = jsonObject.getString("message");

                                        Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();

                                        EmptyField();

                                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                        startActivity(intent);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            String errorString = new String(response.data);
                            try {
                                JSONObject object = new JSONObject(errorString);
                                String message = object.getString("message");
                                Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                                EmptyField();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("nama_lengkap", editTextNamaLengkap.getText().toString());
                        map.put("nomer_telepon", editTextNoHandphone.getText().toString());
                        map.put("email", editTextEmail.getText().toString());
                        map.put("kata_sandi", editTextPassword.getText().toString());
                        map.put("alamat", editTextAlamat.getText().toString());
                        map.put("ktp", editTextNoKtp.getText().toString());
                        map.put("tempat_tanggal_lahir", mTempatTanggaLahir);
                        map.put("jenis_kelamin", mJenisKelamin);
                        Log.d("Map Param", map.toString());
                        return map;
                    }
                };
                requestQueue.add(stringRequest);
            }
        });
    }

    private boolean validation() {

        boolean valid = false;
        //Get values from EditText fields
        String first = editTextNamaLengkap.getText().toString();
        String phone = editTextNoHandphone.getText().toString();
        String email = editTextEmail.getText().toString();
        String pass = editTextPassword.getText().toString();
        String repass = editTextRePassword.getText().toString();
        String noKTP = editTextNoKtp.getText().toString();
        String tempatLahir = editTextTempatLahir.getText().toString();
        String tanggalLahir = editTextTanggalLahir.getText().toString();
        String alamatLengkap = editTextAlamat.getText().toString();

        if (noKTP.isEmpty()) {
            valid = false;
            textInputLayoutNoKTP.setError("Please enter the Id Card");
        } else {
            if (first.length() == 12) {
                valid = true;
                textInputLayoutNoKTP.setError(null);
            } else {
                valid = false;
                textInputLayoutNoKTP.setError("Your Room Number is to short!");
            }
        }

        if (tempatLahir.isEmpty()) {
            valid = false;
            editTextTempatLahir.setError("Please enter Your Place");
        } else {
            if (first.length() >= 4) {
                valid = true;
                editTextTempatLahir.setError(null);
            } else {
                valid = false;
                editTextTempatLahir.setError("Your Place is to short!");
            }
        }
        if (tanggalLahir.isEmpty()) {
            valid = false;
            editTextTanggalLahir.setError("Please enter Your Date");
        } else {
            if (first.length() >= 4) {
                valid = true;
                editTextTanggalLahir.setError(null);
            } else {
                valid = false;
                editTextTanggalLahir.setError("Your Date is to short!");
            }
        }

        if (alamatLengkap.isEmpty()) {
            valid = false;
            textInputLayoutAlamatLengkap.setError("Please enter Your Address");
        } else {
            valid = false;
            textInputLayoutAlamatLengkap.setError("Your Address is to short!");
        }
        //Handling validation for First Name field
        if (first.isEmpty()) {
            valid = false;
            textInputLayoutUsername.setError("Please enter Your Name");
        } else {
            if (first.length() >= 4) {
                valid = true;
                textInputLayoutUsername.setError(null);
            } else {
                valid = false;
                textInputLayoutUsername.setError("Your First Name is to short!");
            }
        }

        //Handling validation for Phone Number Field
        if (phone.isEmpty()) {
            valid = false;
            editTextNoHandphone.setError("Please enter Your Phone Number");
        } else {
            if (phone.length() > 8) {
                valid = true;
                editTextNoHandphone.setError(null);
            } else {
                valid = false;
                editTextNoHandphone.setError("Your Phone Number is to short!");
            }
        }

        //Handling validation for Email Field
        if (email.isEmpty()) {
            valid = false;
            textInputLayoutEmail.setError("Please enter Your Email Address");
        } else {
            //Handling validation for Email field
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                valid = false;
                textInputLayoutEmail.setError("Please enter valid Email!");
            } else {
                valid = true;
                textInputLayoutEmail.setError(null);
            }
        }

        //Handling validation for Password field
        if (pass.isEmpty()) {
            valid = false;
            editTextPassword.setError("Please Enter Valid Password");
        } else {
            if (pass.length() > 5) {
                valid = true;
                editTextPassword.setError(null);
            } else {
                valid = false;
                editTextPassword.setError("Password is to short!");
            }
        }
        //Handling validation for Re-Password field
        if (repass.isEmpty()) {
            valid = false;
            editTextRePassword.setError("Please Enter Valid Re-Password");
        } else {
            if (repass.length() > 5) {
                valid = true;
                editTextRePassword.setError(null);
            } else {
                valid = false;
                editTextRePassword.setError("Password is to short!");
            }
        }
        if (!pass.equals(repass)) {
            valid = false;
            editTextRePassword.setError("Verify your passwords with a password Not Equal!");
        }

        return valid;
    }

    private void EmptyField() {
        editTextNoHandphone.setText("");
        editTextNoKtp.setText("");
        editTextEmail.setText("");
        editTextPassword.setText("");
        editTextRePassword.setText("");
        editTextAlamat.setText("");
        editTextNoKtp.setText("");
        editTextTempatLahir.setText("");
        editTextTanggalLahir.setText("");
    }
}
