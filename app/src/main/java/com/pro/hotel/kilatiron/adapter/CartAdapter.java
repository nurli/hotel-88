package com.pro.hotel.kilatiron.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.activity.EstimasiActivity;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.CartModel;

import org.fabiomsr.moneytextview.MoneyTextView;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    Context context;
    List<CartModel.Datum> cartModelList;
    int id;


    public CartAdapter(Context context, List<CartModel.Datum> cartModelList) {
        this.context = context;
        this.cartModelList = cartModelList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.cart_list_item, parent, false );
        return new ViewHolder( view );
    }


    private CartModel.Datum notifyItemRemoved(CartModel.Datum position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CartModel.Datum datum = cartModelList.get( position );
        holder.textViewNamePerson.setText( datum.getPerson() );
        holder.textViewNote.setText( datum.getNote() );
        holder.textViewQty.setText( String.valueOf( datum.getQty() ) );
        holder.textViewNameMenu.setText( datum.getName() );
        holder.textViewTotalHarga.setAmount( datum.getTotalHargaItem() );
        id = datum.getId();
        Log.d( "Id Cart", String.valueOf( id ) );


        holder.buttonPayment.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( context, EstimasiActivity.class );
                context.startActivity( intent );
            }
        } );
        holder.imageViewDelete.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog( context );
                progressDialog.setMessage( "Loading..." );
                progressDialog.show();
                RequestQueue requestQueue = Volley.newRequestQueue( context );
                StringRequest stringRequest = new StringRequest( Request.Method.DELETE,
                        Config.DELETE_CART_LIST + datum.getId(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject( response );
                                    String message = jsonObject.getString( "message" );
                                    Toast.makeText( context, message, Toast.LENGTH_SHORT ).show();
                                    if (v.equals( holder.imageViewDelete )){
                                        removeAt(position);
                                    }
                                    progressDialog.dismiss();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            String errorString = new String( response.data );
                            try {
                                JSONObject jsonObject = new JSONObject( errorString );
                                String message = jsonObject.getString( "message" );
                                Toast.makeText( context, message, Toast.LENGTH_SHORT ).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }
                        Toast.makeText( context, error.toString(), Toast.LENGTH_SHORT ).show();
                    }
                } );
                requestQueue.add( stringRequest );
            }
        } );
    }

    private void removeAt(int position) {
        cartModelList.remove( position );
        notifyItemRemoved( position );
        notifyItemRangeChanged( position, cartModelList.size() );
    }

    private void paymentOrder() {
        ProgressDialog progressDialog = new ProgressDialog( context );
        progressDialog.setMessage( "Loading..." );
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue( context );
        StringRequest stringRequest = new StringRequest( Request.Method.PATCH, Config.PAYMENT_ORDER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        AlertDialog.Builder builder = new AlertDialog.Builder( context );
                        builder.setTitle( "Pemabayaran" );
                        builder.setMessage( "Apakah Anda ingin melakukan Pembayaran ?" )
                                .setIcon( R.mipmap.ic_launcher_hotel88 )
                                .setCancelable( false )
                                .setPositiveButton( "Ya", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            JSONObject jsonObject = new JSONObject( response );
                                            String message = jsonObject.getString( "message" );
                                            Toast.makeText( context, message, Toast.LENGTH_SHORT ).show();
                                            progressDialog.dismiss();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } )
                                .setNegativeButton( "Tidak", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                } );
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( context, message, Toast.LENGTH_SHORT ).show();
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
//                        map.put( "",  );
                return map;
            }
        };
        requestQueue.add( stringRequest );
    }

    private void dialogResponse() {

    }

    @Override
    public int getItemCount() {
        return cartModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewNameMenu, textViewQty, textViewNote, textViewNamePerson;
        MoneyTextView textViewTotalHarga;
        Button buttonPayment;
        ImageView imageViewDelete;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );

            textViewTotalHarga = itemView.findViewById( R.id.textViewTotalHarga );
            imageViewDelete = itemView.findViewById( R.id.imageViewDelete );
            buttonPayment = itemView.findViewById( R.id.buttonPayment );
            textViewNameMenu = itemView.findViewById( R.id.textViewNameMenu );
            textViewQty = itemView.findViewById( R.id.textViewQuantity );
            textViewNote = itemView.findViewById( R.id.textViewNote );
            textViewNamePerson = itemView.findViewById( R.id.textViewNamePerson );

        }

    }

}
