package com.pro.hotel.kilatiron.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.activity.DetailMenuActivity;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.ImageModel;
import com.pro.hotel.kilatiron.model.MenuModel;
import com.pro.hotel.localcode.utils.PicassoCache;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SliderImageAdapter extends SliderViewAdapter<SliderImageAdapter.ModulHolder> {

    Context context;
    private int mCount;
    MenuModel.Datum datum;
    String path_image;
    List<ImageModel.Datum> datumList = new ArrayList<ImageModel.Datum>(  );

    public SliderImageAdapter(Context context) {
        this.context = context;
    }

    public void setCount(int count) {
        this.mCount = count;
    }

    @Override
    public ModulHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.list_item_image_slider, parent, false );
        return new ModulHolder( view );
    }

    @Override
    public void onBindViewHolder(ModulHolder viewHolder, int position) {
        datum = new MenuModel.Datum();
        int menu_id = datum.getId();

        RequestQueue requestQueue = Volley.newRequestQueue( context);
        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_IMAGE_PATH + menu_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            Log.d("response ", response.toString());
                            JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i = 0; i < data.length(); i++) {
                                ImageModel.Datum _datum = new ImageModel.Datum();
                                JSONObject object = data.getJSONObject( i );
                                _datum.setId( object.getInt( "id" ) );
                                _datum.setName( object.getString( "name" ) );
                                _datum.setPath( object.getString( "path" ) );
                                _datum.setRelationId( object.getInt( "relation_id" ) );
                                datumList.add( _datum);

                                path_image = _datum.getName();
                                Log.d("Path Image ", path_image);

                                PicassoCache.getPicassoInstance( context)
                                        .load( Config.BASE_PATH_URL + "images/menu/" + path_image )
                                        .into( viewHolder.imageViewSlider);
                                Log.d( "Path Image", Config.BASE_PATH_URL + "images/menu/" + path_image);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( context, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );

    }

    @Override
    public int getCount() {
        return mCount;
    }


    class ModulHolder extends SliderViewAdapter.ViewHolder{
        ImageView imageViewSlider;
        View itemView;
        public ModulHolder(View itemView) {
            super( itemView );
            imageViewSlider = itemView.findViewById(R.id.iv_auto_image_slider);
            this.itemView = itemView;

        }
    }
}
