package com.pro.hotel.kilatiron.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("nama_lengkap")
        @Expose
        private String namaLengkap;
        @SerializedName("nomer_telepon")
        @Expose
        private String nomerTelepon;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("jenis_kelamin")
        @Expose
        private Integer jenisKelamin;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("ktp")
        @Expose
        private String ktp;
        @SerializedName("tempat_tanggal_lahir")
        @Expose
        private String tempatTanggalLahir;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNamaLengkap() {
            return namaLengkap;
        }

        public void setNamaLengkap(String namaLengkap) {
            this.namaLengkap = namaLengkap;
        }

        public String getNomerTelepon() {
            return nomerTelepon;
        }

        public void setNomerTelepon(String nomerTelepon) {
            this.nomerTelepon = nomerTelepon;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Integer getJenisKelamin() {
            return jenisKelamin;
        }

        public void setJenisKelamin(Integer jenisKelamin) {
            this.jenisKelamin = jenisKelamin;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getKtp() {
            return ktp;
        }

        public void setKtp(String ktp) {
            this.ktp = ktp;
        }

        public String getTempatTanggalLahir() {
            return tempatTanggalLahir;
        }

        public void setTempatTanggalLahir(String tempatTanggalLahir) {
            this.tempatTanggalLahir = tempatTanggalLahir;
        }

    }

}
