package com.pro.hotel.kilatiron.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.adapter.HistoryPembayaranAdapter;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.HistoryPembayaranModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryPembayaranActivity extends AppCompatActivity {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.recyclerViewPembayaran)
    RecyclerView recyclerViewPembayaran;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    List<HistoryPembayaranModel.Datum> historyPembayaranModels;
    HistoryPembayaranAdapter adapter;
    SharedPreferences sharedPreferences;
    String id_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_history_pembayaran );
        ButterKnife.bind( this );

        historyPembayaranModels = new ArrayList<>(  );

        sharedPreferences = getSharedPreferences( Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );
        id_user = sharedPreferences.getString( Config.ID_USER, "Not Available" );

        loadData();

        imageViewBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                @SuppressLint("WrongConstant")
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( HistoryPembayaranActivity.this, LinearLayoutManager.VERTICAL, false );
                recyclerViewPembayaran.setLayoutManager( layoutManager );
                adapter = new HistoryPembayaranAdapter( HistoryPembayaranActivity.this, historyPembayaranModels );
                recyclerViewPembayaran.setAdapter( adapter );
                swipeRefreshLayout.setRefreshing( false );
            }
        } );


    }

    private void loadData() {
        RequestQueue requestQueue = Volley.newRequestQueue( this );
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                Config.GET_PAYMENT_HISTORY + id_user,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                                JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i=0; i<data.length(); i++){
                                HistoryPembayaranModel.Datum datum = new HistoryPembayaranModel.Datum();
                                JSONObject object = data.getJSONObject( i );
                                datum.setCreatedAt( object.getString( "createdAt" ) );
                                datum.setTotalQty( object.getString( "total_qty" ) );
                                datum.setStatus( object.getString( "status" ) );
                                datum.setInvoice( object.getString( "invoice" ) );
                                datum.setTotalHarga( object.getString( "total_harga" ) );
                                datum.setNomerTelepon( object.getString( "nomer_telepon" ) );
                                datum.setNamaLengkap( object.getString( "nama_lengkap" ) );
                                historyPembayaranModels.add( datum );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loadDataRecycler(historyPembayaranModels);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( HistoryPembayaranActivity.this, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );
    }

    private void loadDataRecycler(List<HistoryPembayaranModel.Datum> historyPembayaranModels) {
        @SuppressLint("WrongConstant")
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( HistoryPembayaranActivity.this, LinearLayoutManager.VERTICAL, false );
        recyclerViewPembayaran.setLayoutManager( layoutManager );
        adapter = new HistoryPembayaranAdapter( HistoryPembayaranActivity.this, historyPembayaranModels );
        recyclerViewPembayaran.setAdapter( adapter );
    }
}
