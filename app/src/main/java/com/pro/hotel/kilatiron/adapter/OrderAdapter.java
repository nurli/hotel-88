package com.pro.hotel.kilatiron.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.GsonBuilder;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.activity.DetailOrderActivity;
import com.pro.hotel.kilatiron.model.OrderModel;

import org.fabiomsr.moneytextview.MoneyTextView;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    Context context;
    List<OrderModel.Datum> orderModels;

    public OrderAdapter(Context context, List<OrderModel.Datum> orderModels) {
        this.context = context;
        this.orderModels = orderModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.list_item_order, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderModel.Datum orderModel = orderModels.get( position );
        holder.textViewInvoice.setText( orderModel.getInvoice() );
        holder.textViewHargaTotal.setAmount( Float.parseFloat( orderModel.getHargaTotal() ) );

        if (orderModel.getStatus().equals( 1 ) ){
            holder.textViewStatus.setText( "Unpaid" );
        }else {
            holder.textViewStatus.setText( "Paid" );
        }

        holder.textViewEstimasi.setText( orderModel.getEstimasi() );

//        holder.itemView.setOnClickListener( new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent( context, DetailOrderActivity.class );
//                intent.putExtra( "order", new GsonBuilder().create().toJson( orderModel ) );
//                context.startActivity( intent );
//            }
//        } );
    }

    @Override
    public int getItemCount() {
        return orderModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewInvoice, textViewEstimasi, textViewStatus;
        MoneyTextView textViewHargaTotal;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            textViewEstimasi = itemView.findViewById( R.id.textViewEstimasi );
            textViewInvoice = itemView.findViewById( R.id.textViewInvoice );
            textViewStatus = itemView.findViewById( R.id.textViewStatus );
            textViewHargaTotal = itemView.findViewById( R.id.textViewHargaTotal);
        }
    }
}
