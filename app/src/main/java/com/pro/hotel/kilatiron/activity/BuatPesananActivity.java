package com.pro.hotel.kilatiron.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.adapter.MenuAdapter;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.ImageModel;
import com.pro.hotel.kilatiron.model.MenuModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuatPesananActivity extends AppCompatActivity {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.editTextSearch)
    EditText editTextSearch;
    @BindView(R.id.recyclerViewFood)
    RecyclerView recyclerViewFood;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    List<MenuModel.Datum> menuModels;
    List<ImageModel.Datum> imageModels;

    MenuAdapter menuAdapter;
    SharedPreferences sharedPreferences;
    int id;
    String namePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_buat_pesanan );
        ButterKnife.bind( this );

        menuModels = new ArrayList<>();
        imageModels = new ArrayList<>();
        sharedPreferences = getSharedPreferences( Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );

        loadData();

//        loadImagesValue();

        imageViewBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager( BuatPesananActivity.this, 2 );
                recyclerViewFood.setLayoutManager( layoutManager );
                menuAdapter = new MenuAdapter( BuatPesananActivity.this, menuModels );
                recyclerViewFood.setAdapter( menuAdapter );
                swipeRefreshLayout.setRefreshing( false );
            }
        } );

        editTextSearch.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter( s.toString() );
            }
        } );
    }

    private void filter(String toString) {
        ArrayList<MenuModel.Datum> datumArrayList = new ArrayList<>();
        for (MenuModel.Datum datum : menuModels) {
            if (datum.getLabel().toLowerCase().contains( toString.toLowerCase() )){
                datumArrayList.add( datum );
            }
        }
        menuAdapter.filterList( datumArrayList );
    }

    private void loadImagesValue() {
        Log.d( "Image Path ", Config.GET_IMAGE_PATH + id );
        RequestQueue requestQueue = Volley.newRequestQueue( BuatPesananActivity.this );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_IMAGE_PATH + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i = 0; i < data.length(); i++) {
                                ImageModel.Datum datum = new ImageModel.Datum();
                                JSONObject object = data.getJSONObject( i );
                                datum.setId( object.getInt( "id" ) );
                                datum.setName( object.getString( "name" ) );
                                datum.setPath( object.getString( "path" ) );
                                datum.setRelationId( object.getInt( "relation_id" ) );
                                namePath = datum.getName();
                                imageModels.add( datum );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( BuatPesananActivity.this, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );
    }

    private void loadData() {
        RequestQueue requestQueue = Volley.newRequestQueue( this );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_MENU,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject( response );
//                            JSONObject path = jsonObject.getJSONObject( "path" );
                            JSONArray data = jsonObject.getJSONArray( "data" );

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject( i );
                                MenuModel.Datum datum = new MenuModel.Datum();
                                datum.setId( object.getInt( "id" ) );
                                datum.setLabel( object.getString( "label" ) );
                                datum.setKategoriMenuId( object.getInt( "kategori_menu_id" ) );
                                datum.setHarga( object.getInt( "harga" ) );
                                datum.setDeskripsi( object.getString( "deskripsi" ) );
                                datum.setKategori( object.getString( "kategori" ) );
                                datum.setCover( object.getString( "cover" ) );
                                menuModels.add( datum );

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString( Config.ID_MENU, String.valueOf( id ) );
                                editor.commit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        setUp( menuModels );
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject object = new JSONObject( errorString );
                        String message = object.getString( "message" );
                        Toast.makeText( BuatPesananActivity.this, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager( this, 2 );
        recyclerViewFood.setLayoutManager( layoutManager );
        menuAdapter = new MenuAdapter( this, menuModels );
        recyclerViewFood.setAdapter( menuAdapter );

    }

    private void setUp(List<MenuModel.Datum> menuModels) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager( BuatPesananActivity.this, 2 );
        recyclerViewFood.setLayoutManager( layoutManager );
        menuAdapter = new MenuAdapter( BuatPesananActivity.this, menuModels );
        recyclerViewFood.setAdapter( menuAdapter );
    }


}
