package com.pro.hotel.kilatiron.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public boolean setSuccess(Boolean success) {
        this.success = success;
        return false;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("total_harga_item")
        @Expose
        private Integer totalHargaItem;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("qty")
        @Expose
        private Integer qty;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("person")
        @Expose
        private String person;
        @SerializedName("person_id")
        @Expose
        private Integer personId;

        public Integer getTotalHargaItem() {
            return totalHargaItem;
        }

        public void setTotalHargaItem(Integer totalHargaItem) {
            this.totalHargaItem = totalHargaItem;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getQty() {
            return qty;
        }

        public void setQty(Integer qty) {
            this.qty = qty;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getPerson() {
            return person;
        }

        public void setPerson(String person) {
            this.person = person;
        }

        public Integer getPersonId() {
            return personId;
        }

        public void setPersonId(Integer personId) {
            this.personId = personId;
        }

    }

}
