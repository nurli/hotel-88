package com.pro.hotel.kilatiron.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.adapter.SliderImageAdapter;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.ImageModel;
import com.pro.hotel.kilatiron.model.MenuModel;
import com.pro.hotel.localcode.utils.PicassoCache;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.fabiomsr.moneytextview.MoneyTextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailMenuActivity extends AppCompatActivity {

    @BindView(R.id.imageViewFood)
    ImageView imageViewFood;
    @BindView(R.id.textViewNameFood)
    TextView textViewNameFood;
    @BindView(R.id.textViewSellingPrice)
    MoneyTextView textViewPrice;
    @BindView(R.id.textViewDescription)
    TextView textViewDeskripsi;
    @BindView(R.id.buttonAddCart2)
    Button buttonAdd;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView( R.id.imageSlider )
    SliderView sliderView;

    MenuModel.Datum menuModel;
    int menu_id;
    List<ImageModel.Datum> imageModels = new ArrayList<>(  );

    String path_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_detail_menu );
        ButterKnife.bind( this );

        menuModel = new GsonBuilder().create().fromJson( getIntent().getStringExtra( "menu" ), MenuModel.Datum.class );

        textViewNameFood.setText( menuModel.getLabel() );
        textViewDeskripsi.setText( menuModel.getDeskripsi() );
        textViewPrice.setAmount( menuModel.getHarga() );
        menu_id = menuModel.getId();
        String cover = menuModel.getCover();
        PicassoCache.getPicassoInstance( DetailMenuActivity.this )
                .load( Config.BASE_PATH_URL + "images/menu/" + cover )
                .into( imageViewFood);

//        final SliderImageAdapter sliderImageAdapter = new SliderImageAdapter( this );
//        sliderImageAdapter.setCount(imageModels.size());
//
//        sliderView.setSliderAdapter( sliderImageAdapter );
//        sliderView.setIndicatorAnimation( IndicatorAnimations.WORM ); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
//        sliderView.setSliderTransformAnimation( SliderAnimations.SIMPLETRANSFORMATION );
//        sliderView.setAutoCycleDirection( SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH );
//        sliderView.setIndicatorSelectedColor( Color.WHITE );
//        sliderView.setIndicatorUnselectedColor( Color.GRAY );
//        sliderView.setScrollTimeInSec( 4 ); //set scroll delay in seconds :
//        sliderView.startAutoCycle();

//        RequestQueue requestQueue = Volley.newRequestQueue( DetailMenuActivity.this );
//        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_IMAGE_PATH + menu_id,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject jsonObject = new JSONObject( response );
//                            JSONArray data = jsonObject.getJSONArray( "data" );
//                            for (int i = 0; i < data.length(); i++) {
//                                ImageModel.Datum datum = new ImageModel.Datum();
//                                JSONObject object = data.getJSONObject( i );
//                                datum.setId( object.getInt( "id" ) );
//                                datum.setName( object.getString( "name" ) );
//                                datum.setPath( object.getString( "path" ) );
//                                datum.setRelationId( object.getInt( "relation_id" ) );
//                                path_image = datum.getName();
//                                PicassoCache.getPicassoInstance( DetailMenuActivity.this )
//                                        .load( Config.BASE_PATH_URL + "images/menu/" + path_image )
//                                        .error( R.drawable.ic_no_food )
//                                        .into( imageViewFood);
//                                Log.d( "myPath ", path_image );
//                                Log.d( "Path Image", Config.BASE_PATH_URL + "images/menu/" + path_image);
//                                imageModels.add( datum );
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                NetworkResponse response = error.networkResponse;
//                if (response != null && response.data != null) {
//                    String errorString = new String( response.data );
//                    try {
//                        JSONObject jsonObject = new JSONObject( errorString );
//                        String message = jsonObject.getString( "message" );
//                        Toast.makeText( DetailMenuActivity.this, message, Toast.LENGTH_SHORT ).show();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        } );
//        requestQueue.add( stringRequest );

        imageViewBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );
        buttonAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( DetailMenuActivity.this, TotalPesanan.class );
                intent.putExtra( "nama_food", textViewNameFood.getText().toString().trim() );
                intent.putExtra( "deskripsi", textViewDeskripsi.getText().toString().trim() );
                intent.putExtra( "harga", String.valueOf( textViewPrice.getAmount() ) );
                intent.putExtra( "kategori_menu_id", menuModel.getKategoriMenuId() );
                intent.putExtra( "menu_id", String.valueOf( menu_id ) );
                intent.putExtra( "cover", cover );
                startActivity( intent );
            }
        } );

    }
}
