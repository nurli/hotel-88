package com.pro.hotel.kilatiron.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.localcode.activity.TotalPesanan;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EstimasiActivity extends AppCompatActivity {

    @BindView( R.id.buttonSimpanEstimasi )
    Button buttonSimpanEstimasi;
    @BindView( R.id.editTextEstimasiSampai )
    EditText editTextEstimasi;
    @BindView( R.id.imageViewTime )
    ImageView imageViewTime;

    private TimePickerDialog timePickerDialog;
    private SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_estimasi );
        ButterKnife.bind( this );

        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        imageViewTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog = new TimePickerDialog( EstimasiActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        editTextEstimasi.setText(hourOfDay + ":" + minute);
                    }
                }, 0, 0, true);
                timePickerDialog.show();
            }
        });



        buttonSimpanEstimasi.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(EstimasiActivity.this );
                progressDialog.setMessage( "Loading..." );
                progressDialog.show();
                RequestQueue requestQueue = Volley.newRequestQueue( EstimasiActivity.this );
                StringRequest stringRequest = new StringRequest( Request.Method.POST, Config.ADD_ORDER,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject( response );
                                    boolean success = jsonObject.getBoolean( "success" );
                                    String message = jsonObject.getString( "message" );
                                    Toast.makeText( EstimasiActivity.this, message, Toast.LENGTH_SHORT ).show();
//                                    paymentOrder();
                                    editTextEstimasi.setText( "" );
                                    Intent intent = new Intent( EstimasiActivity.this,  PaymentActivity.class);
                                    startActivity( intent );
                                    finish();
                                    progressDialog.dismiss();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            String errorString = new String( response.data );
                            try {
                                JSONObject jsonObject = new JSONObject( errorString );
                                String message = jsonObject.getString( "message" );
                                Toast.makeText( EstimasiActivity.this, message, Toast.LENGTH_SHORT ).show();
                                progressDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } ) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        SharedPreferences sharedPreferences = getSharedPreferences( Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );
                        String id_user = sharedPreferences.getString( Config.ID_USER, "Not Availablae" );
                        Map<String, String> map = new HashMap<String, String>();
                        map.put( "tamu_id", id_user );
                        map.put( "estimasi", editTextEstimasi.getText().toString() );
                        return map;
                    }
                };
                requestQueue.add( stringRequest );
            }
        } );
    }
}
