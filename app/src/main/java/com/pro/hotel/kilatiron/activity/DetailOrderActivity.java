package com.pro.hotel.kilatiron.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.adapter.DetailOrderAdapter;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.DetailOrderModel;
import com.pro.hotel.kilatiron.model.HistoryPembayaranModel;
import com.pro.hotel.kilatiron.model.OrderModel;

import org.fabiomsr.moneytextview.MoneyTextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailOrderActivity extends AppCompatActivity {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.buttonBayar)
    Button buttonBayar;
    @BindView(R.id.textViewPembayaranID)
    TextView textViewPembayaranID;
    @BindView(R.id.textViewLableMenu)
    TextView textViewLableMenu;
    @BindView(R.id.textViewQuantity)
    TextView textViewQuantity;
    @BindView(R.id.textViewTotalHarga)
    MoneyTextView textViewTOtalHarga;
    @BindView(R.id.textViewCatatan)
    TextView textViewCatatan;
    @BindView(R.id.textViewHargaSatuan)
    MoneyTextView textViewHargaSatuan;
    @BindView(R.id.recyclerViewDetailOrder)
    RecyclerView recyclerViewDetailOrder;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.textViewTotalQuantity)
    TextView textViewQuantityKeseluruhan;
    @BindView(R.id.textViewHargaKeseluruhan)
    MoneyTextView textViewHargaKeseluruhan;


    SharedPreferences sharedPreferences;
    DetailOrderAdapter detailOrderAdapter;
    List<DetailOrderModel.Datum> detailOrderModelList;
    String id_user;
    HistoryPembayaranModel.Datum datum;
    String inv, quantity_keseluruhan;
    int harga_keseluruhan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        ButterKnife.bind(this);


        detailOrderModelList = new ArrayList<DetailOrderModel.Datum>();

        datum = new GsonBuilder().create().fromJson(getIntent().getStringExtra("order"), HistoryPembayaranModel.Datum.class);
        String status = datum.getStatus();

        inv = datum.getInvoice();
        harga_keseluruhan = Integer.parseInt(datum.getTotalHarga());
        quantity_keseluruhan = datum.getTotalQty();

        textViewQuantityKeseluruhan.setText(quantity_keseluruhan);
        textViewHargaKeseluruhan.setAmount(harga_keseluruhan);

        if (status.equals("1")) {
            buttonBayar.setVisibility(View.VISIBLE);
        } else {
            buttonBayar.setVisibility(View.GONE);
        }

        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        id_user = sharedPreferences.getString(Config.ID_USER, "Not Available");
        Log.d("ID User ", id_user);

        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                @SuppressLint("WrongConstant") RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DetailOrderActivity.this, LinearLayoutManager.VERTICAL, false);
                recyclerViewDetailOrder.setLayoutManager(layoutManager);
                detailOrderAdapter = new DetailOrderAdapter(DetailOrderActivity.this, detailOrderModelList);
                recyclerViewDetailOrder.setAdapter(detailOrderAdapter);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        Log.d("My Invoice ", datum.getInvoice());
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                Config.GET_DETAIL_ORDER + datum.getInvoice(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray data = jsonObject.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject datas = data.getJSONObject(i);
                                textViewCatatan.setText(datas.getString("catatan"));
                                textViewPembayaranID.setText(datas.getString("pembayaran_id"));
                                textViewLableMenu.setText(datas.getString("label"));
                                textViewQuantity.setText(datas.getString("qty"));
                                textViewTOtalHarga.setAmount(datas.getInt("total_harga_item"));
                                textViewHargaSatuan.setAmount(datas.getInt("harga_satuan"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String(response.data);
                    try {
                        JSONObject objcet = new JSONObject(errorString);
                        String message = objcet.getString("message");
                        Toast.makeText(DetailOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        requestQueue.add(stringRequest);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue requestQueue1 = Volley.newRequestQueue(DetailOrderActivity.this);
                StringRequest stringRequest1 = new StringRequest(
                        Request.Method.PATCH,
                        Config.PAYMENT_ORDER + datum.getInvoice(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String message = jsonObject.getString("message");
                                    Toast.makeText(DetailOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(DetailOrderActivity.this, Dashboard.class);
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            String errorString = new String(response.data);
                            try {
                                JSONObject jsonObject = new JSONObject(errorString);
                                String message = jsonObject.getString("message");
                                Toast.makeText(DetailOrderActivity.this, message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                requestQueue1.add(stringRequest1);
            }
        });
    }

    private void loadData() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_DETAIL_ORDER + datum.getInvoice(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray data = jsonObject.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                DetailOrderModel.Datum datas = new DetailOrderModel.Datum();
                                JSONObject object = data.getJSONObject(i);
                                datas.setCatatan(object.getString("catatan"));
                                datas.setHargaSatuan(object.getInt("harga_satuan"));
                                datas.setPembayaranId(object.getString("pembayaran_id"));
                                datas.setLabel(object.getString("label"));
                                datas.setQty(object.getInt("qty"));
                                datas.setTotalHargaItem(object.getInt("total_harga_item"));
                                detailOrderModelList.add(datas);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loaddataRecycler(detailOrderModelList);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String(response.data);
                    try {
                        JSONObject jsonObject = new JSONObject(errorString);
                        String message = jsonObject.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        requestQueue.add(stringRequest);
    }

    private void loaddataRecycler(List<DetailOrderModel.Datum> detailOrderModelList) {
        @SuppressLint("WrongConstant")
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DetailOrderActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerViewDetailOrder.setLayoutManager(layoutManager);
        detailOrderAdapter = new DetailOrderAdapter(DetailOrderActivity.this, detailOrderModelList);
        recyclerViewDetailOrder.setAdapter(detailOrderAdapter);
    }
}
