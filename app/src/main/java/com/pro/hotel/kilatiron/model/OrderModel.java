package com.pro.hotel.kilatiron.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {

        @SerializedName("invoice")
        @Expose
        private String invoice;
        @SerializedName("tamu_id")
        @Expose
        private Integer tamuId;
        @SerializedName("estimasi")
        @Expose
        private String estimasi;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("harga_total")
        @Expose
        private String hargaTotal;

        public String getInvoice() {
            return invoice;
        }

        public void setInvoice(String invoice) {
            this.invoice = invoice;
        }

        public Integer getTamuId() {
            return tamuId;
        }

        public void setTamuId(Integer tamuId) {
            this.tamuId = tamuId;
        }

        public String getEstimasi() {
            return estimasi;
        }

        public void setEstimasi(String estimasi) {
            this.estimasi = estimasi;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getHargaTotal() {
            return hargaTotal;
        }

        public void setHargaTotal(String hargaTotal) {
            this.hargaTotal = hargaTotal;
        }

    }

}