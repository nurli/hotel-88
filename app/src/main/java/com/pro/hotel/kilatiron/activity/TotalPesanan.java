package com.pro.hotel.kilatiron.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.ImageModel;
import com.pro.hotel.localcode.activity.BuatPesanan;
import com.pro.hotel.localcode.utils.PicassoCache;

import org.fabiomsr.moneytextview.MoneyTextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TotalPesanan extends AppCompatActivity {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.imageViewFood)
    ImageView imageViewFood;
    @BindView(R.id.buttonAdd)
    Button buttonAdd;
    @BindView(R.id.buttonLess)
    Button buttonLess;
    @BindView(R.id.textViewCount)
    TextView textViewCount;
    @BindView(R.id.editTextCatatan)
    EditText editTextCatatan;
    @BindView(R.id.buttonAddCart)
    Button buttonKeranjang;
    @BindView(R.id.textViewPurchasePrice)
    MoneyTextView textViewTotalHarga;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    int sum, count;
    float harga;
    String id_user, id;
    SharedPreferences sharedPreferences;
    int menu_id;
    List<ImageModel.Datum> imageModels = new ArrayList<>(  );

    String path_image;

    private TimePickerDialog timePickerDialog;
    private SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_total_pesanan2 );
        ButterKnife.bind( this );

        sharedPreferences = getSharedPreferences( Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );
        id_user = sharedPreferences.getString( Config.ID_USER, "Not Available" );
        Log.d( "User ", id_user );

        Intent intent = getIntent();
        String title = intent.getStringExtra( "nama_food" );
        String desk = intent.getStringExtra( "deskripsi" );
        harga = Float.parseFloat( intent.getStringExtra( "harga" ) );
        int kategori_menu_id = intent.getIntExtra( "kategori_menu_id", 0 );
        id = intent.getStringExtra( "menu_id" );
        String cover = intent.getStringExtra( "cover" );
        PicassoCache.getPicassoInstance( TotalPesanan.this )
                .load( Config.BASE_PATH_URL + "images/menu/" + cover )
                .error( R.drawable.ic_no_food )
                .into( imageViewFood);

        /*RequestQueue requestQueue = Volley.newRequestQueue( TotalPesanan.this );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_IMAGE_PATH + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i = 0; i < data.length(); i++) {
                                ImageModel.Datum datum = new ImageModel.Datum();
                                JSONObject object = data.getJSONObject( i );
                                datum.setId( object.getInt( "id" ) );
                                datum.setName( object.getString( "name" ) );
                                datum.setPath( object.getString( "path" ) );
                                datum.setRelationId( object.getInt( "relation_id" ) );
                                path_image = datum.getName();
                                PicassoCache.getPicassoInstance( TotalPesanan.this )
                                        .load( Config.BASE_PATH_URL + "images/menu/" + path_image )
                                        .error( R.drawable.ic_no_food )
                                        .into( imageViewFood);
                                Log.d( "myPath ", path_image );
                                Log.d( "Path Image", Config.BASE_PATH_URL + "images/menu/" + path_image);
                                imageModels.add( datum );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( TotalPesanan.this, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );*/

        textViewTitle.setText( title );
        textViewTotalHarga.setAmount( harga );

        buttonAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 10) {
                    count = 10;
                    textViewCount.setText( String.valueOf( count ) );
                } else {
                    count = count + 1;
                    textViewCount.setText( String.valueOf( count ) );
                    sum = (int) (count * harga);
                    textViewTotalHarga.setAmount( sum );
                }
            }
        } );

        buttonLess.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 1) {
                    count = 1;
                    textViewCount.setText( String.valueOf( count ) );
                } else {
                    count = count - 1;
                    textViewCount.setText( String.valueOf( count ) );
                    sum = (int) (count * harga);
                    textViewTotalHarga.setAmount( sum );
                    Log.d( "Count ", String.valueOf( count ) );

                }
            }
        } );
        buttonKeranjang.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AlertDialog.Builder builder = new AlertDialog.Builder( TotalPesanan.this );
//                LayoutInflater inflater = getLayoutInflater();
//                View view = inflater.inflate( R.layout.dialog_layout_estimasi, null );
//                builder.setView( view );
//                builder.setTitle( "Estimasi" );
//                builder.setMessage( "Tentukan estimasi sampai jika Anda diluar Hotel" );
//                builder.setIcon( R.mipmap.ic_launcher_hotel88 );
//                builder.setCancelable( false );
//
//                EditText editTextEstimasi = view.findViewById( R.id.editTextEstimasiSampai );
//                ImageView imageViewTime = view.findViewById( R.id.imageViewTime );
//
//                simpleDateFormat = new SimpleDateFormat( "dd-MM-yyyy", Locale.US );
//
//                imageViewTime.setOnClickListener( new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        timePickerDialog = new TimePickerDialog( TotalPesanan.this, new TimePickerDialog.OnTimeSetListener() {
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                                editTextEstimasi.setText( hourOfDay + ":" + minute );
//                            }
//                        }, 0, 0, true );
//                        timePickerDialog.show();
//
//                    }
//                } );
//
//                builder.setPositiveButton( "Simpan", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        ProgressDialog progressDialog = new ProgressDialog( TotalPesanan.this );
//                        progressDialog.setMessage( "Loading..." );
//                        progressDialog.show();
//
//                        RequestQueue requestQueue = Volley.newRequestQueue( TotalPesanan.this );
//                        StringRequest stringRequest = new StringRequest( Request.Method.POST, Config.ADD_ORDER,
//                                new Response.Listener<String>() {
//                                    @Override
//                                    public void onResponse(String response) {
//                                        try {
//                                            JSONObject jsonObject = new JSONObject( response );
//                                            boolean success = jsonObject.getBoolean( "success" );
//                                            String message = jsonObject.getString( "message" );
//                                            Toast.makeText( TotalPesanan.this, message, Toast.LENGTH_SHORT ).show();
//                                            if (success){
//                                                addKeranjang();
//                                            }else {
//                                                Toast.makeText( TotalPesanan.this, "Order Gagal!!", Toast.LENGTH_SHORT ).show();
//                                            }
//                                            progressDialog.dismiss();
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                }, new Response.ErrorListener() {
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                NetworkResponse response = error.networkResponse;
//                                if (response != null && response.data != null) {
//                                    String errorString = new String( response.data );
//                                    try {
//                                        JSONObject jsonObject = new JSONObject( errorString );
//                                        String message = jsonObject.getString( "message" );
//                                        Toast.makeText( TotalPesanan.this, message, Toast.LENGTH_SHORT ).show();
//                                        progressDialog.dismiss();
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }
//                        } ) {
//                            @Override
//                            protected Map<String, String> getParams() throws AuthFailureError {
//                                Map<String, String> map = new HashMap<String, String>();
//                                map.put( "tamu_id", id_user );
//                                map.put( "estimasi", editTextEstimasi.getText().toString() );
//                                return map;
//                            }
//                        };
//                        requestQueue.add( stringRequest );
//
//                    }
//                } );
//                builder.setNegativeButton( "Batal", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                } );
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
                addKeranjang();
            }
        } );

        imageViewBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );
    }

    private void addKeranjang() {
        ProgressDialog progressDialog = new ProgressDialog( TotalPesanan.this );
        progressDialog.setMessage( "Loading..." );
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue( TotalPesanan.this );
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                com.pro.hotel.kilatiron.helper.Config.ADD_CART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            String message = jsonObject.getString( "message" );
                            Toast.makeText( TotalPesanan.this, message, Toast.LENGTH_SHORT ).show();
//                                    EmptyField();
                            // TODO get data Cart in Cart Activity
                            Intent intent1 = new Intent( TotalPesanan.this, Dashboard.class );
                            startActivity( intent1 );
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject object = new JSONObject( errorString );
                        String message = object.getString( "message" );
                        Toast.makeText( TotalPesanan.this, message, Toast.LENGTH_SHORT ).show();
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put( "menu_id", id );
                map.put( "qty", textViewCount.getText().toString() );
                map.put( "catatan", editTextCatatan.getText().toString() );
                map.put( "tamu_id", id_user );
                Log.d( "Parameter keranjang ", map.toString() );
                return map;
            }
        };
        requestQueue.add( stringRequest );
    }

    private void EmptyField() {
        editTextCatatan.setText( "" );
        textViewCount.setText( 0 );
    }
}
