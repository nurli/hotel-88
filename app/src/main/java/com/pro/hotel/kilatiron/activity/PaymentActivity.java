package com.pro.hotel.kilatiron.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.adapter.OrderAdapter;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.recyclerViewPembayaran)
    RecyclerView recyclerViewPembayaran;
    @BindView(R.id.buttonPayment)
    Button buttonPayment;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    List<OrderModel.Datum> orderModels;
    OrderAdapter orderAdapter;
    SharedPreferences sharedPreferences;
    String id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_payment );
        ButterKnife.bind( this );


        orderModels = new ArrayList<>();

        sharedPreferences = getSharedPreferences( Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );
        id_user = sharedPreferences.getString( Config.ID_USER, "Not Available" );
        loadData();

        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                @SuppressLint("WrongConstant")
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( PaymentActivity.this, LinearLayoutManager.VERTICAL, false );
                recyclerViewPembayaran.setLayoutManager( layoutManager );
                orderAdapter = new OrderAdapter( PaymentActivity.this, orderModels );
                recyclerViewPembayaran.setAdapter( orderAdapter );
                swipeRefreshLayout.setRefreshing( false );
            }
        } );


        imageViewBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        buttonPayment.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        } );
    }

    private void loadData() {
        RequestQueue requestQueue = Volley.newRequestQueue( this );
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                Config.GET_ORDDR,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i = 0; i < data.length(); i++) {
                                OrderModel.Datum datum = new OrderModel.Datum();
                                JSONObject object = data.getJSONObject( i );
                                datum.setInvoice( object.getString( "invoice" ) );
                                datum.setEstimasi( object.getString( "estimasi" ) );
                                datum.setHargaTotal( object.getString( "harga_total" ) );
                                datum.setStatus( object.getInt( "status" ) );
                                datum.setTamuId( object.getInt( "tamu_id" ) );
                                orderModels.add( datum );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loadDataRecycler( orderModels );
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( PaymentActivity.this, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );
    }

    private void loadDataRecycler(List<OrderModel.Datum> orderModels) {
        @SuppressLint("WrongConstant")
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( PaymentActivity.this, LinearLayoutManager.VERTICAL, false );
        recyclerViewPembayaran.setLayoutManager( layoutManager );
        orderAdapter = new OrderAdapter( PaymentActivity.this, orderModels );
        recyclerViewPembayaran.setAdapter( orderAdapter );
    }
}
