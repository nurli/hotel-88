package com.pro.hotel.kilatiron.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.GsonBuilder;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.activity.DetailOrderActivity;
import com.pro.hotel.kilatiron.model.HistoryPembayaranModel;

import org.fabiomsr.moneytextview.MoneyTextView;

import java.util.List;

public class HistoryPembayaranAdapter extends RecyclerView.Adapter<HistoryPembayaranAdapter.ViewHolder> {

    Context context;
    List<HistoryPembayaranModel.Datum> datumList;

    public HistoryPembayaranAdapter(Context context, List<HistoryPembayaranModel.Datum> datumList) {
        this.context = context;
        this.datumList = datumList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_history_pembayaran, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistoryPembayaranModel.Datum datum = datumList.get(position);
//        int _status = datum.getStatus();
//
//        if (_status == 1) {
//            holder.textViewStatus.setText("Unpaid");
//        } else {
//            holder.textViewStatus.setText("Paid");
//        }
//        holder.textViewStatus.setText(datum.getStatus());
        holder.textViewStatus.setText( String.valueOf( datum.getStatus() )  );
        holder.textViewInvoice.setText(datum.getInvoice());
        holder.textViewCreateAt.setText(datum.getCreatedAt());
        holder.textViewQty.setText(datum.getTotalQty());
        holder.textViewTotalHarga.setAmount(Float.parseFloat(datum.getTotalHarga()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailOrderActivity.class);
                intent.putExtra( "order", new GsonBuilder().create().toJson( datum ) );
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewInvoice, textViewEstimasi, textViewStatus, textViewCreateAt, textViewQty;
        MoneyTextView textViewTotalHarga;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewCreateAt = itemView.findViewById(R.id.textViewCreateAt);
            textViewEstimasi = itemView.findViewById(R.id.textViewEstimasi);
            textViewInvoice = itemView.findViewById(R.id.textViewInvoice);
            textViewStatus = itemView.findViewById(R.id.textViewStatus);
            textViewQty = itemView.findViewById(R.id.textViewQty);
            textViewTotalHarga = itemView.findViewById(R.id.textViewTotalHarga);
        }
    }
}
