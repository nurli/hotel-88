package com.pro.hotel.kilatiron.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class MenuModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("cover")
        @Expose
        private String cover;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("kategori_menu_id")
        @Expose
        private Integer kategoriMenuId;
        @SerializedName("harga")
        @Expose
        private Integer harga;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;
        @SerializedName("kategori")
        @Expose
        private String kategori;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCover() {
            return cover;
        }

        public void setCover(String cover) {
            this.cover = cover;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getKategoriMenuId() {
            return kategoriMenuId;
        }

        public void setKategoriMenuId(Integer kategoriMenuId) {
            this.kategoriMenuId = kategoriMenuId;
        }

        public Integer getHarga() {
            return harga;
        }

        public void setHarga(Integer harga) {
            this.harga = harga;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getKategori() {
            return kategori;
        }

        public void setKategori(String kategori) {
            this.kategori = kategori;
        }

    }

}