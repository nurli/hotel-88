package com.pro.hotel.kilatiron.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pro.hotel.R;
import com.pro.hotel.kilatiron.model.DetailOrderModel;

import org.fabiomsr.moneytextview.MoneyTextView;

import java.util.List;

public class DetailOrderAdapter extends RecyclerView.Adapter<DetailOrderAdapter.ViewHolder> {

    Context context;
    List<DetailOrderModel.Datum> detailOrderModelList;

    public DetailOrderAdapter(Context context, List<DetailOrderModel.Datum> detailOrderModelList) {
        this.context = context;
        this.detailOrderModelList = detailOrderModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.list_item_detail_order, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DetailOrderModel.Datum datum = detailOrderModelList.get( position );
        holder.textViewHargaSatuan.setAmount( datum.getHargaSatuan() );
        holder.textViewTOtalHarga.setAmount( datum.getTotalHargaItem() );
        holder.textViewPembayaranID.setText( datum.getPembayaranId() );
        holder.textViewLableMenu.setText( datum.getLabel() );
        holder.textViewCatatan.setText( datum.getCatatan() );
        holder.textViewQuantity.setText( String.valueOf( datum.getQty() ) );
    }

    @Override
    public int getItemCount() {
        return detailOrderModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewPembayaranID;
        TextView textViewLableMenu;
        TextView textViewQuantity;
        MoneyTextView textViewTOtalHarga;
        TextView textViewCatatan;
        MoneyTextView textViewHargaSatuan;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            textViewCatatan = itemView.findViewById( R.id.textViewPembayaranID );
            textViewLableMenu = itemView.findViewById( R.id.textViewLableMenu );
            textViewCatatan = itemView.findViewById( R.id.textViewCatatan );
            textViewQuantity = itemView.findViewById( R.id.textViewQuantity );
            textViewPembayaranID = itemView.findViewById( R.id.textViewPembayaranID );
            textViewTOtalHarga = itemView.findViewById( R.id.textViewTotalHarga );
            textViewHargaSatuan = itemView.findViewById( R.id.textViewHargaSatuan );

        }
    }
}
