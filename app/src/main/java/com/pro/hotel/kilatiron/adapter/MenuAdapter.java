package com.pro.hotel.kilatiron.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.activity.DetailMenuActivity;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.ImageModel;
import com.pro.hotel.kilatiron.model.MenuModel;
import com.pro.hotel.localcode.utils.PicassoCache;

import org.fabiomsr.moneytextview.MoneyTextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHOlder> {

    Context context;
    List<MenuModel.Datum> menuModels;
    List<ImageModel.Datum> imageModels = new ArrayList<>(  );

    String path_image;
    int menu_id;

    public MenuAdapter(Context context, List<MenuModel.Datum> menuModels) {
        this.context = context;
        this.menuModels = menuModels;
    }


    @NonNull
    @Override
    public ViewHOlder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.list_view_user_food, parent, false );
        return new ViewHOlder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHOlder holder, int position) {
        MenuModel.Datum menuModel = menuModels.get( position );
        MenuModel menuModel1 = new MenuModel();

        holder.textViewTitle.setText( menuModel.getLabel() );
        holder.textViewHarga.setAmount( menuModel.getHarga() );
        String cover = menuModel.getCover();
        Log.d("Cover ", cover);
        menu_id = menuModel.getId();
        Log.d( "ID Menu", String.valueOf( menu_id ) );

        PicassoCache.getPicassoInstance( context )
                .load( Config.BASE_PATH_URL + "images/menu/" + cover )

                .into( holder.imageViewMenu );

        /*RequestQueue requestQueue = Volley.newRequestQueue( context );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_IMAGE_PATH + menu_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i = 0; i < data.length(); i++) {
                                ImageModel.Datum datum = new ImageModel.Datum();
                                JSONObject object = data.getJSONObject( i );
                                datum.setId( object.getInt( "id" ) );
                                datum.setName( object.getString( "name" ) );
                                datum.setPath( object.getString( "path" ) );
                                datum.setRelationId( object.getInt( "relation_id" ) );
                                path_image = datum.getName();
                                PicassoCache.getPicassoInstance( context )
                                        .load( Config.BASE_PATH_URL + "images/menu/" + path_image )
                                        .error( R.drawable.ic_no_food )
                                        .into( holder.imageViewMenu );
                                Log.d( "myPath ", path_image );
                                Log.d( "Path Image", Config.BASE_PATH_URL + "images/menu/" + path_image);
                                imageModels.add( datum );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( context, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );*/

//        PicassoCache.getPicassoInstance( context )
//                .load( Config.BASE_URL + "images/menu/" + path_image )
//                .error( R.drawable.ic_no_food )
//                .into( holder.imageViewMenu );
        holder.itemView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( context, DetailMenuActivity.class );
                intent.putExtra( "menu", new GsonBuilder().create().toJson( menuModel ) );
                context.startActivity( intent );
            }
        } );
    }

    private void getImages() {
        RequestQueue requestQueue = Volley.newRequestQueue( context );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_IMAGE_PATH + menu_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i = 0; i < data.length(); i++) {
                                ImageModel.Datum datum = new ImageModel.Datum();
                                JSONObject object = data.getJSONObject( i );
                                datum.setId( object.getInt( "id" ) );
                                datum.setName( object.getString( "name" ) );
                                datum.setPath( object.getString( "path" ) );
                                datum.setRelationId( object.getInt( "relation_id" ) );
                                path_image = datum.getName();
                                imageModels.add( datum );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject jsonObject = new JSONObject( errorString );
                        String message = jsonObject.getString( "message" );
                        Toast.makeText( context, message, Toast.LENGTH_SHORT ).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    class ViewHOlder extends RecyclerView.ViewHolder {
        ImageView imageViewMenu;
        TextView textViewTitle;
        MoneyTextView textViewHarga;

        public ViewHOlder(@NonNull View itemView) {
            super( itemView );
            imageViewMenu = itemView.findViewById( R.id.imageViewFood );
            textViewTitle = itemView.findViewById( R.id.textViewNameFood );
            textViewHarga = itemView.findViewById( R.id.textViewPurchasePrice );
        }
    }

    public void filterList(ArrayList<MenuModel.Datum> filterList){
        menuModels = filterList;
        notifyDataSetChanged();
    }
}
