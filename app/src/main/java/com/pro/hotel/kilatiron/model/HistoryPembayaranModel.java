package com.pro.hotel.kilatiron.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoryPembayaranModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("invoice")
        @Expose
        private String invoice;
        @SerializedName("nama_lengkap")
        @Expose
        private String namaLengkap;
        @SerializedName("nomer_telepon")
        @Expose
        private String nomerTelepon;
        @SerializedName("total_harga")
        @Expose
        private String totalHarga;
        @SerializedName("total_qty")
        @Expose
        private String totalQty;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("status")
        @Expose
        private String status;

        public String getInvoice() {
            return invoice;
        }

        public void setInvoice(String invoice) {
            this.invoice = invoice;
        }

        public String getNamaLengkap() {
            return namaLengkap;
        }

        public void setNamaLengkap(String namaLengkap) {
            this.namaLengkap = namaLengkap;
        }

        public String getNomerTelepon() {
            return nomerTelepon;
        }

        public void setNomerTelepon(String nomerTelepon) {
            this.nomerTelepon = nomerTelepon;
        }

        public String getTotalHarga() {
            return totalHarga;
        }

        public void setTotalHarga(String totalHarga) {
            this.totalHarga = totalHarga;
        }

        public String getTotalQty() {
            return totalQty;
        }

        public void setTotalQty(String totalQty) {
            this.totalQty = totalQty;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }


}