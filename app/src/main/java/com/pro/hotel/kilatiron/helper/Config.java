package com.pro.hotel.kilatiron.helper;

public class Config {

    public static final String SHARED_PREF_NAME = "myloginapp";
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";

    public static final String BASE_URL = "http://docker25229-env-1848087.kilatiron.com/api/v1/";
    public static final String BASE_PATH_URL = "http://docker25229-env-1848087.kilatiron.com/";

    public static final String REGISTER = "auth/register";
    public static final String LOGIN = "auth/login";
    public static final String ID_USER = "id_user";
    public static final String NAMA_LENGKAP = "nama_lengkap";
    public static final String NOMOR_TEL = "nomor_telp";
    public static final String EMAIL = "email";
    public static final String JENID_KELAMIN = "jenis_kelamin";
    public static final String ALAMAT = "alamat";
    public static final String KTP = "ktp";
    public static final String TMP_TGL_LAHIR = "tempat_tanggal_lahir";

    public static final String GET_MENU = BASE_URL + "menu";
    public static final String ID_MENU = "id_menu";
    public static final String ADD_CART = BASE_URL + "order/add/cart";
    public static final String PATH_IMAGE_MENU = "";
    public static final String GET_IMAGE_MENU = BASE_URL + PATH_IMAGE_MENU;

    public static final String GET_CART = BASE_URL+ "order/cart/list/";
    public static final String DELETE_CART_LIST = BASE_URL + "order/cart/delete/";
    public static final String PAYMENT_ORDER = BASE_URL + "order/paid/";
    public static final String ADD_ORDER = BASE_URL + "order/add";
    public static final String GET_ORDDR = BASE_URL + "order/";
    public static final String GET_DETAIL_ORDER = BASE_URL + "order/";
    public static final String GET_PAYMENT_HISTORY = BASE_URL + "order/paid/history/";
    public static final String GET_IMAGE_PATH = BASE_URL + "menu/images/detail-relation/";
}
