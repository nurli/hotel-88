package com.pro.hotel.kilatiron.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.adapter.CartAdapter;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.kilatiron.model.CartModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartActivity extends AppCompatActivity {

    @BindView(R.id.recyclerViewCart)
    RecyclerView recyclerViewCart;
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.textStatusData)
    TextView textViewStatus;
    @BindView(R.id.buttonPayment)
    Button buttonPayment;

    List<CartModel.Datum> cartModels;
    CartModel cartModel;
    CartAdapter cartAdapter;
    SharedPreferences sharedPreferences;
    String id_user;
    boolean isSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cart );
        ButterKnife.bind( this );

        cartModels = new ArrayList<>();

        sharedPreferences = getSharedPreferences( Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );
        id_user = sharedPreferences.getString( Config.ID_USER, "Not Available" );
        Log.d( "ID_USER ", id_user );

        loadData();

        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                @SuppressLint("WrongConstant")
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( CartActivity.this, LinearLayoutManager.VERTICAL, false );
                recyclerViewCart.setLayoutManager( layoutManager );
                cartAdapter = new CartAdapter( CartActivity.this, cartModels );
                recyclerViewCart.setAdapter( cartAdapter );
                swipeRefreshLayout.setRefreshing( false );
            }
        } );
//        refreshData();


        buttonPayment.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshData();

                Log.d( "Cart Model Size", String.valueOf( cartModels.size() ) );
                if (cartModels.size() == 0) {
                    Toast.makeText( CartActivity.this, "Anda belum melakukan pesanan", Toast.LENGTH_SHORT ).show();
                } else {
                    Intent intent = new Intent( CartActivity.this, EstimasiActivity.class );
                    startActivity( intent );
                }
            }
        } );
        imageViewBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

    }

    public void refreshData() {
        @SuppressLint("WrongConstant") RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( CartActivity.this, LinearLayoutManager.VERTICAL, false );
        recyclerViewCart.setLayoutManager( layoutManager );
        cartAdapter = new CartAdapter( CartActivity.this, cartModels );
        recyclerViewCart.setAdapter( cartAdapter );
    }

    private void loadData() {
        // TODO show the cart
        RequestQueue requestQueue = Volley.newRequestQueue( this );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, Config.GET_CART + id_user,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject( response );
                            Log.d( "Json Cart ", response.toLowerCase() );

                            isSuccess = jsonObject.getBoolean( "success" );
                            Log.d( "success", String.valueOf( isSuccess ) );

                            if (isSuccess == false) {
                                buttonPayment.setVisibility( View.GONE );
                            } else {
                                buttonPayment.setVisibility( View.VISIBLE );
                            }

                            JSONArray data = jsonObject.getJSONArray( "data" );
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonObject1 = data.getJSONObject( i );
                                CartModel.Datum datum = new CartModel.Datum();
                                datum.setId( jsonObject1.getInt( "id" ) );
                                datum.setName( jsonObject1.getString( "name" ) );
                                datum.setQty( jsonObject1.getInt( "qty" ) );
                                datum.setNote( jsonObject1.getString( "note" ) );
                                datum.setPerson( jsonObject1.getString( "person" ) );
                                datum.setPersonId( jsonObject1.getInt( "person_id" ) );
                                datum.setTotalHargaItem( jsonObject1.getInt( "total_harga_item" ) );
                                cartModels.add( datum );
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loadDataRecycler( cartModels );
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    String errorString = new String( response.data );
                    try {
                        JSONObject object = new JSONObject( errorString );
                        String message = object.getString( "message" );
                        boolean success = object.getBoolean( "success" );
                        if (success == true) {
                            textViewStatus.setVisibility( View.VISIBLE );
                            textViewStatus.setText( message );
                            buttonPayment.setVisibility( View.GONE );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } );
        requestQueue.add( stringRequest );
    }

    public void deleteCart() {
        cartModels = new ArrayList<>();
        if (cartModels.size() > 0) {
            cartModels.remove( cartModels.size() - 1 );
            cartAdapter.notifyDataSetChanged();
//            cartAdapter.notifyItemRangeChanged( position, cartModels.size() );
        }
    }

    private void loadDataRecycler(List<CartModel.Datum> cartModels) {
        @SuppressLint("WrongConstant")
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( CartActivity.this, LinearLayoutManager.VERTICAL, false );
        recyclerViewCart.setLayoutManager( layoutManager );
        cartAdapter = new CartAdapter( CartActivity.this, cartModels );
        recyclerViewCart.setAdapter( cartAdapter );
    }
}
