package com.pro.hotel.kilatiron.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;
import com.pro.hotel.R;
import com.pro.hotel.kilatiron.RegisterActivity;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.localcode.activity.Login;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.tilEmail)
    TextInputLayout textInputLayoutEmail;
    @BindView(R.id.tilPassword)
    TextInputLayout textInputLayoutPassword;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextPassword)
    EditText editTextPasword;
    @BindView(R.id.buttonLogIn)
    Button buttonLogin;
    @BindView(R.id.textViewForgotPassword)
    TextView textViewForgotPassword;
    @BindView(R.id.textViewRegister)
    TextView textViewRegisterl;

    SharedPreferences sharedPreferences;


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_APPEND);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                    StringRequest stringRequest = new StringRequest(
                            Request.Method.POST,
                            Config.BASE_URL + Config.LOGIN,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        boolean success = jsonObject.getBoolean("success");
                                        String message = jsonObject.getString("message");
                                        JSONObject data = jsonObject.getJSONObject("data");
                                        String id = data.getString("id");
                                        String namaLengkap = data.getString("nama_lengkap");
                                        String nomorTel = data.getString("nomer_telepon");
                                        String email = data.getString("email");
                                        String jenis_kelamin = data.getString("jenis_kelamin");
                                        String alamat = data.getString("alamat");
                                        String ktp = data.getString("ktp");
                                        String tmpTglLahir = data.getString("tempat_tanggal_lahir");

                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, true);
                                        editor.putString(Config.ID_USER, id);
                                        editor.putString(Config.NAMA_LENGKAP, namaLengkap);
                                        editor.putString(Config.NOMOR_TEL, nomorTel);
                                        editor.putString(Config.EMAIL, email);
                                        editor.putString(Config.JENID_KELAMIN, jenis_kelamin);
                                        editor.putString(Config.ALAMAT, alamat);
                                        editor.putString(Config.KTP, ktp);
                                        editor.putString(Config.TMP_TGL_LAHIR, tmpTglLahir);
                                        editor.commit();


                                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                        EmptyField();

                                        Intent intent = new Intent(LoginActivity.this, Dashboard.class);
                                        startActivity(intent);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.data != null) {
                                String errorString = new String(response.data);
                                try {
                                    JSONObject object = new JSONObject(errorString);
                                    String message = object.getString("message");
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                    EmptyField();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<String, String>();
                            map.put("email", editTextEmail.getText().toString());
                            map.put("kata_sandi", editTextPasword.getText().toString());
                            return map;
                        }
                    };
                    requestQueue.add(stringRequest);
                }
            }
        });
    }

    private void EmptyField() {
        editTextEmail.setText("");
        editTextPasword.setText("");
    }

    private boolean validation() {
        boolean valid = false;

        //Get values from EditText fields
        String User = editTextEmail.getText().toString();
        String Password = editTextPasword.getText().toString();

        //Handling validation for Email field
        if (User.isEmpty()) {
            valid = false;
            textInputLayoutEmail.setError("Please enter valid email!");
        } else {
            valid = true;
            textInputLayoutEmail.setError(null);
        }

        //Handling validation for Password field
        if (Password.isEmpty()) {
            valid = false;
            textInputLayoutPassword.setError("Please enter valid password!");
        } else {
            if (Password.length() > 5) {
                valid = true;
                textInputLayoutPassword.setError(null);
            } else {
                valid = false;
                textInputLayoutPassword.setError("Password is to short!");
            }
        }

        return valid;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        boolean loggedin = sharedPreferences.getBoolean(Config.LOGGEDIN_SHARED_PREF, false);

        if (loggedin == true) {
//            Intent intent = new Intent(Login.this, Dashboard.class);
            Intent intent = new Intent(LoginActivity.this, Dashboard.class);
            startActivity(intent);
            finish();
        } else if (loggedin == false) {

        }
    }
}
