package com.pro.hotel.kilatiron.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pro.hotel.R;
import com.pro.hotel.kilatiron.adapter.CartAdapter;
import com.pro.hotel.kilatiron.helper.Config;
import com.pro.hotel.localcode.activity.Login;
import com.pro.hotel.localcode.activity.MainUser;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Dashboard extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageViewLogOut)
    ImageView imageViewLogout;
    @BindView(R.id.buttonBuatPesanan)
    Button buttonBuatPesanan;
    @BindView(R.id.buttonHistoriPesanan)
    Button buttonHistorPesanan;
    @BindView(R.id.buttonHistoriPembayaran)
    Button buttonHistoriPembayaran;
    @BindView(R.id.textViewNAmaUser)
    TextView textViewNamaUser;
    @BindView(R.id.imageViewCart)
    ImageView imageViewCart;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_dashboard );
        ButterKnife.bind( this );

        sharedPreferences = getSharedPreferences( Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );
        String nama = sharedPreferences.getString( Config.NAMA_LENGKAP, "Not Available" );
        textViewNamaUser.setText( nama );

        imageViewCart.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Dashboard.this, PaymentActivity.class );
                startActivity( intent );
            }
        } );
        buttonBuatPesanan.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Dashboard.this, BuatPesananActivity.class );
                startActivity( intent );
            }
        } );
        buttonHistorPesanan.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Dashboard.this, CartActivity.class );
                startActivity( intent );
            }
        } );
        buttonHistoriPembayaran.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Dashboard.this, HistoryPembayaranActivity.class );
                startActivity( intent );
            }
        } );
        imageViewLogout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogOutProgress();
            }
        } );

    }

    private void LogOutProgress() {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder( this );
        alert.setTitle( "Log Out" );
        alert.setMessage( "Are you sure you want to Log Out?" )
                .setPositiveButton( "Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences sharedPreferences = getSharedPreferences( com.pro.hotel.localcode.utils.Config.SHARED_PREF_NAME, Context.MODE_PRIVATE );
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean( com.pro.hotel.localcode.utils.Config.LOGGEDIN_SHARED_PREF, false );
                                editor.commit();
                                Intent intent = new Intent( Dashboard.this, LoginActivity.class );
                                startActivity( intent );
                                finish();
                            }
                        } )
                .setNegativeButton( "No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        } );
        androidx.appcompat.app.AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }


}
