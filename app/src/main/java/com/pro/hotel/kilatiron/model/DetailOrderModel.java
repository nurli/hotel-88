package com.pro.hotel.kilatiron.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailOrderModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("pembayaran_id")
        @Expose
        private String pembayaranId;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("qty")
        @Expose
        private Integer qty;
        @SerializedName("harga_satuan")
        @Expose
        private Integer hargaSatuan;
        @SerializedName("total_harga_item")
        @Expose
        private Integer totalHargaItem;
        @SerializedName("catatan")
        @Expose
        private String catatan;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPembayaranId() {
            return pembayaranId;
        }

        public void setPembayaranId(String pembayaranId) {
            this.pembayaranId = pembayaranId;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getQty() {
            return qty;
        }

        public void setQty(Integer qty) {
            this.qty = qty;
        }

        public Integer getHargaSatuan() {
            return hargaSatuan;
        }

        public void setHargaSatuan(Integer hargaSatuan) {
            this.hargaSatuan = hargaSatuan;
        }

        public Integer getTotalHargaItem() {
            return totalHargaItem;
        }

        public void setTotalHargaItem(Integer totalHargaItem) {
            this.totalHargaItem = totalHargaItem;
        }

        public String getCatatan() {
            return catatan;
        }

        public void setCatatan(String catatan) {
            this.catatan = catatan;
        }

    }

}