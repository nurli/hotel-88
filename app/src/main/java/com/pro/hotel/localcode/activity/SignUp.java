package com.pro.hotel.localcode.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.pro.hotel.R;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.UserModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SignUp extends AppCompatActivity {

    TextInputLayout tilUsename, tilNomorKamar, tilJenisKelamin, tilTempatLahir, tilTanggalLahir, tilPhoneNumber, tilAgama, tilAlamatLengkap, tilEmail, tilPassword, tilRePassword;
    EditText editTextUsername, editTextNomorKamar, editTextJenisKelamin, editTextTempatLahir, editTextTanggalLahir, editTextPhoneNumber, editTextAgama, editTextAlamatLengkap, editTextEmail, editTextPassword, editTextRePassword;
    Button buttonAddUser, buttonViewList;
    ImageView imageViewBack, imageViewDate;
    Spinner spinnerGender, spinnerreligion;
    TextView textViewLogIn;
    Toolbar toolbar;

    SQLiteHelper sqLiteHelper;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat simpleDateFormat;

    String _gender, _religion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        sqLiteHelper = new SQLiteHelper(this);

        buttonViewList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUp.this, UsersList.class);
                startActivity(intent);
            }
        });

        final String[] gender = new String[]{
                "Laki-laki", "Perempuan"
        };
        List<String> list = new ArrayList<>(Arrays.asList(gender));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, list
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapter);

        final String[] religion = new String[]{
                "Islam", "Katolik", "Protestan", "Budha", "Hindu"
        };
        List<String> listReligion = new ArrayList<>(Arrays.asList(religion));
        ArrayAdapter<String> adapaterReligion = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, listReligion
        );
        adapaterReligion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerreligion.setAdapter(adapaterReligion);


        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        imageViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                datePickerDialog = new DatePickerDialog(SignUp.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, dayOfMonth);
                        editTextTanggalLahir.setText(simpleDateFormat.format(newDate.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AddUser();
                AlertDialog.Builder alert = new AlertDialog.Builder(SignUp.this);
                alert.setTitle("Save");
                alert.setMessage("Are you sure you want to Save this Data?")
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (validate()) {
                                            if (!sqLiteHelper.isUserExists(editTextUsername.getText().toString())) {
                                                String username = editTextUsername.getText().toString();
                                                String no_kamar = editTextNomorKamar.getText().toString();
                                                String jns_kel = String.valueOf(spinnerGender.getSelectedItem());
                                                String tempat_lahir = editTextTempatLahir.getText().toString();
                                                String tanggal_lahir = editTextTanggalLahir.getText().toString();
                                                String phoneNumber = editTextPhoneNumber.getText().toString();
                                                String agama = String.valueOf(spinnerreligion.getSelectedItem());
                                                String address = editTextAlamatLengkap.getText().toString();
                                                String level = "2";
                                                sqLiteHelper.AddUser(new UserModel(null, username, no_kamar, jns_kel, tempat_lahir, tanggal_lahir, phoneNumber, agama, address, level));
                                                Toast.makeText(SignUp.this, "Sing Up is Success", Toast.LENGTH_SHORT).show();
                                                emptyField();
/*                        Intent intent = new Intent(SignUp.this, Login.class);
                        startActivity(intent);*/
                                            } else {
                                                Toast.makeText(SignUp.this, "Sign Up is Failed !!!", Toast.LENGTH_SHORT).show();
                                                emptyField();
                                            }
                                        }
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });

        textViewLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUp.this, Login.class);
                startActivity(intent);
            }
        });


    }

    private void emptyField() {
        editTextUsername.setText("");
        editTextNomorKamar.setText("");
        editTextTempatLahir.setText("");
        editTextTanggalLahir.setText("");
        editTextPhoneNumber.setText("");
        editTextAlamatLengkap.setText("");
    }

    private boolean validate() {
        boolean valid = false;
        //Get values from EditText fields
        String first = editTextUsername.getText().toString();
        String phone = editTextPhoneNumber.getText().toString();
        String email = editTextEmail.getText().toString();
        String pass = editTextPassword.getText().toString();
        String repass = editTextRePassword.getText().toString();
        String noKamar = editTextNomorKamar.getText().toString();
        String jenisKelamin = editTextJenisKelamin.getText().toString();
        String tempatLahir = editTextTempatLahir.getText().toString();
        String tanggalLahir = editTextTanggalLahir.getText().toString();
        String agama = editTextAgama.getText().toString();
        String alamatLengkap = editTextAlamatLengkap.getText().toString();

        if (noKamar.isEmpty()) {
            valid = false;
            tilNomorKamar.setError("Please enter the Room NUmber");
        } else {
            if (first.length() >= 3) {
                valid = true;
                tilNomorKamar.setError(null);
            } else {
                valid = false;
                tilNomorKamar.setError("Your Room Number is to short!");
            }
        }
        /*if (jenisKelamin.isEmpty()) {
            valid = false;
            tilJenisKelamin.setError("Please enter Gender");
        } else {
            if (first.length() >= 5) {
                valid = true;
                tilJenisKelamin.setError(null);
            } else {
                valid = false;
                tilJenisKelamin.setError("Your Gender Name is to short!");
            }
        }*/
        if (tempatLahir.isEmpty()) {
            valid = false;
            tilTempatLahir.setError("Please enter Your Place");
        } else {
            if (first.length() >= 4) {
                valid = true;
                tilTempatLahir.setError(null);
            } else {
                valid = false;
                tilTempatLahir.setError("Your Place is to short!");
            }
        }
        if (tanggalLahir.isEmpty()) {
            valid = false;
            tilTanggalLahir.setError("Please enter Your Date");
        } else {
            if (first.length() >= 4) {
                valid = true;
                tilTanggalLahir.setError(null);
            } else {
                valid = false;
                tilTanggalLahir.setError("Your Date is to short!");
            }
        }
        /*if (agama.isEmpty()) {
            valid = false;
            tilAgama.setError("Please enter Your Religion");
        } else {
            if (first.length() >= 4) {
                valid = true;
                tilAgama.setError(null);
            } else {
                valid = false;
                tilAgama.setError("Your Religion is to short!");
            }
        }*/
        if (alamatLengkap.isEmpty()) {
            valid = false;
            tilAlamatLengkap.setError("Please enter Your Address");
        } else {
            valid = false;
            tilAlamatLengkap.setError("Your Address is to short!");
        }
        //Handling validation for First Name field
        if (first.isEmpty()) {
            valid = false;
            tilUsename.setError("Please enter Your Name");
        } else {
            if (first.length() >= 4) {
                valid = true;
                tilUsename.setError(null);
            } else {
                valid = false;
                tilUsename.setError("Your First Name is to short!");
            }
        }

        //Handling validation for Phone Number Field
        if (phone.isEmpty()) {
            valid = false;
            tilPhoneNumber.setError("Please enter Your Phone Number");
        } else {
            if (phone.length() > 8) {
                valid = true;
                tilPhoneNumber.setError(null);
            } else {
                valid = false;
                tilPhoneNumber.setError("Your Phone Number is to short!");
            }
        }
        //Handling validation for Email Field
       /* if (email.isEmpty()) {
            valid = false;
            tilEmail.setError("Please enter Your Email Address");
        } else {
            //Handling validation for Email field
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                valid = false;
                tilEmail.setError("Please enter valid Email!");
            } else {
                valid = true;
                tilEmail.setError(null);
            }
        }

        //Handling validation for Password field
        if (pass.isEmpty()) {
            valid = false;
            tilPassword.setError("Please Enter Valid Password");
        } else {
            if (pass.length() > 5) {
                valid = true;
                tilPassword.setError(null);
            } else {
                valid = false;
                tilPassword.setError("Password is to short!");
            }
        }
        //Handling validation for Re-Password field
        if (repass.isEmpty()) {
            valid = false;
            tilRePassword.setError("Please Enter Valid Re-Password");
        } else {
            if (repass.length() > 5) {
                valid = true;
                tilRePassword.setError(null);
            } else {
                valid = false;
                tilRePassword.setError("Password is to short!");
            }
        }
        if (!pass.equals(repass)) {
            valid = false;
            tilRePassword.setError("Verify your passwords with a password Not Equal!");
        }*/
        return valid;
    }

    private void init() {
        buttonViewList = findViewById(R.id.buttonViewList);
        imageViewDate = findViewById(R.id.imageViewDate);
        spinnerGender = findViewById(R.id.spinnerGender);
        spinnerreligion = findViewById(R.id.spinnerReligion);
        imageViewBack = findViewById(R.id.imageViewBack);
        toolbar = findViewById(R.id.toolbar);
        tilUsename = findViewById(R.id.tilUsername);
        tilPhoneNumber = findViewById(R.id.tilNoTel);
        tilEmail = findViewById(R.id.tilEmail);
        tilPassword = findViewById(R.id.tilPassword);
        tilRePassword = findViewById(R.id.tilRePassword);
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPhoneNumber = findViewById(R.id.editTextNotel);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextRePassword = findViewById(R.id.editTextRepassword);
        buttonAddUser = findViewById(R.id.buttonSignUp);
        textViewLogIn = findViewById(R.id.textViewLogin);
        tilNomorKamar = findViewById(R.id.tilNoKamar);
        tilJenisKelamin = findViewById(R.id.tilJenisKelamin);
        tilTempatLahir = findViewById(R.id.tilTempatLahir);
        tilTanggalLahir = findViewById(R.id.tilTanggalLahir);
        tilAgama = findViewById(R.id.tilAgama);
        tilAlamatLengkap = findViewById(R.id.tilAlamatLengkap);
        editTextNomorKamar = findViewById(R.id.editTextNoKamar);
        editTextJenisKelamin = findViewById(R.id.editTextJenisKelamin);
        editTextTempatLahir = findViewById(R.id.editTextTempatLahir);
        editTextTanggalLahir = findViewById(R.id.editTextTanggalLahir);
        editTextAgama = findViewById(R.id.editTextAgama);
        editTextAlamatLengkap = findViewById(R.id.editTextAlamatLengkap);
    }
}
