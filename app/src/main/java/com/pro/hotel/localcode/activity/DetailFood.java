package com.pro.hotel.localcode.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.textfield.TextInputLayout;
import com.pro.hotel.R;
import com.pro.hotel.localcode.database.SQLiteHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DetailFood extends AppCompatActivity {

    Spinner spinnerFoodCategory;
    TextInputLayout tilFodName, tilQuantity, tilPurchasePrice, tilSellingPrice, tilDescription;
    EditText editTextFoodName, editTextQuantity, editTextPurchasePrice, editTextSellingPrice, editTextDescription;
    ImageView imageViewFood;
    Button buttonEdit, buttonDelete;
    String id;
    TextView textViewNameImage;

    SQLiteHelper sqLiteHelper;
    Bitmap bitmap;

    private final int GALLERY = 1;
    int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_food);

        init();

        sqLiteHelper = new SQLiteHelper(this);
        ActivityCompat.requestPermissions(DetailFood.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        Intent intent = getIntent();
        String nameFood = intent.getStringExtra("name");
        String quantity = intent.getStringExtra("quantity");
        String purchasePrise = intent.getStringExtra("purchase_price");
        String sellingPrice = intent.getStringExtra("selling_price");
        String desc = intent.getStringExtra("description");
        id = intent.getStringExtra("id");

        editTextFoodName.setText(nameFood);
        editTextQuantity.setText(quantity);
        editTextPurchasePrice.setText(purchasePrise);
        editTextSellingPrice.setText(sellingPrice);
        editTextDescription.setText(desc);

        Bundle bundle = getIntent().getExtras();
        byte[] bytes = bundle.getByteArray("image_food");
        Bitmap bitmap1 = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        imageViewFood.setImageBitmap(bitmap1);


        imageViewFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseImage();
            }
        });
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {

                    ProgressEditFood();
                }
            }
        });
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDeleteFood(id);
            }
        });
    }

    private void ProgressDeleteFood(final String id) {
        AlertDialog.Builder alert = new AlertDialog.Builder(DetailFood.this);
        alert.setTitle("Delete");
        alert.setMessage("Are you sure you want to Delete this Item?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sqLiteHelper.deleteFood(id);
                                Toast.makeText(DetailFood.this, "Delete Item Success", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    private void ProgressEditFood() {
        Drawable drawable = imageViewFood.getDrawable();
        bitmap = ((BitmapDrawable) drawable).getBitmap();

        File path = Environment.getExternalStorageDirectory();
        File dir = new File(path + "/FoodImage/");
        dir.mkdir();

        String filename = String.format(editTextFoodName.getText().toString() + ".jpg", System.currentTimeMillis());
        File file = new File(dir, filename);

                /*if (filename != null) {
                    Toast.makeText(this, "Adding is Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Adding is Failed", Toast.LENGTH_SHORT).show();
                    EmptyField();
                }*/

        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri uri = Uri.parse(file.getAbsolutePath());
        imageViewFood.setImageURI(uri);
        textViewNameImage.setText("Image Saved in internal storage.\n " + uri);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte imageFood[] = byteArrayOutputStream.toByteArray();

        String foodName = editTextFoodName.getText().toString();
        String quantity = editTextQuantity.getText().toString();
        String purchasePrice = editTextPurchasePrice.getText().toString();
        String sellingPrice = editTextSellingPrice.getText().toString();
        String desc = editTextDescription.getText().toString();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        String strTime = simple.format(calendar.getTime());
        String date = strTime;

        boolean isUpdate = sqLiteHelper.EditFoodd(id, foodName, quantity, purchasePrice, sellingPrice, imageFood, date, desc);
        if (isUpdate == true) {
            Toast.makeText(this, "Edit Success", Toast.LENGTH_SHORT).show();
            onBackPressed();
            EmptyField();
        } else {
            Toast.makeText(this, "Edit Failed!!!", Toast.LENGTH_SHORT).show();
        }

        /*if (!sqLiteHelper.isFoodExists(editTextFoodName.getText().toString())) {

            *//*sqLiteHelper.EditFood(id, foodName, quantity, purchasePrice, sellingPrice, imageFood, date);
            Toast.makeText(this, "Edit Success", Toast.LENGTH_SHORT).show();
            onBackPressed();
            EmptyField();*//*
        } else {
            Toast.makeText(this, "Edit Failed!!!, Please Use another Name of Food", Toast.LENGTH_SHORT).show();
//            EmptyField();
        }*/
    }

    private void EmptyField() {
        editTextSellingPrice.setText("");
        editTextFoodName.setText("");
        editTextPurchasePrice.setText("");
        editTextQuantity.setText("");
        imageViewFood.setImageResource(R.drawable.ic_food);
    }

    private boolean validate() {
        boolean valid = false;
        //Get values from EditText fields
        String foodName = editTextFoodName.getText().toString();
        String quantity = editTextQuantity.getText().toString();
        String purchasePrice = editTextPurchasePrice.getText().toString();
        String sellingPrice = editTextSellingPrice.getText().toString();
        String desc = editTextDescription.getText().toString();

        if (desc.isEmpty()) {
            valid = false;
            tilDescription.setError("Please enter your description");
        } else {
            valid = true;
            tilDescription.setError(null);
        }
        //Handling validation for First Name field
        if (foodName.isEmpty()) {
            valid = false;
            tilFodName.setError("Please enter your Food Name");
        } else {
            if (foodName.length() >= 4) {
                valid = true;
                tilFodName.setError(null);
            } else {
                valid = false;
                tilFodName.setError("Your First Name is to short!");
            }
        }

        //Handling validation for Phone Number Field
        if (quantity.isEmpty()) {
            valid = false;
            tilQuantity.setError("Please enter Quntity");
        } else {
            if (quantity.length() > 0) {
                valid = true;
                tilQuantity.setError(null);
            } else {
                valid = false;
                tilQuantity.setError("Your Quantity is to short!");
            }
        }
        //Handling validation for Email Field
        if (purchasePrice.isEmpty()) {
            valid = false;
            tilPurchasePrice.setError("Please enter Purchase Price");
        } else {
            if (quantity.length() > 0) {
                valid = true;
                tilQuantity.setError(null);
            } else {
                valid = false;
                tilQuantity.setError("The Purchase Price  is to short!");
            }
        }

        //Handling validation for Password field
        if (sellingPrice.isEmpty()) {
            valid = false;
            tilSellingPrice.setError("Please Enter Selling Price");
        } else {
            if (sellingPrice.length() > 0) {
                valid = true;
                tilSellingPrice.setError(null);
            } else {
                valid = false;
                tilSellingPrice.setError("The Selling Price is to short!");
            }
        }

        return valid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == PICK_IMAGE_REQUEST) {
            if (data != null) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    imageViewFood.setImageBitmap(bitmap);
//                    upload(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Failed !!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (requestCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
//                    upload(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Failed !!!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void ChooseImage() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void init() {
        spinnerFoodCategory = findViewById(R.id.spinnerFoodCategory);
        editTextFoodName = findViewById(R.id.editTextFoodName);
        editTextQuantity = findViewById(R.id.editTextQuantity);
        editTextPurchasePrice = findViewById(R.id.editTextPurchasePrice);
        editTextSellingPrice = findViewById(R.id.editTextSellingPrice);
        imageViewFood = findViewById(R.id.imageViewFood);
        buttonEdit = findViewById(R.id.buttonEdit);
        buttonDelete = findViewById(R.id.buttonDelete);
        tilFodName = findViewById(R.id.tilFoodName);
        tilQuantity = findViewById(R.id.tilQuanity);
        tilPurchasePrice = findViewById(R.id.tilPurchasePrice);
        tilSellingPrice = findViewById(R.id.tilSellingPrice);
        tilDescription = findViewById(R.id.tilDescription);
        editTextDescription = findViewById(R.id.editTextDescription);
        textViewNameImage = findViewById(R.id.textViewNameImage);
    }
}
