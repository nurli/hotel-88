package com.pro.hotel.localcode.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.pro.hotel.R;
import com.pro.hotel.localcode.activity.DetailFood;
import com.pro.hotel.localcode.activity.FoodList;
import com.pro.hotel.localcode.activity.OrderFood;
import com.pro.hotel.localcode.model.FoodModel;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class UserFoodAdapter extends ArrayAdapter<FoodModel> {
    Context context;
    private ArrayList<FoodModel> foodModelArrayList;

    public UserFoodAdapter(Context context, ArrayList<FoodModel> foodModelArrayList){
        super(context, R.layout.list_view_user_food, foodModelArrayList);
        this.context = context;
        this.foodModelArrayList = foodModelArrayList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final FoodModel foodModel = getItem(position);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = ((Activity) context).getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.list_view_user_food, parent, false);
            viewHolder.imageViewFood = convertView.findViewById(R.id.imageViewFood);
            viewHolder.textViewFoodName = convertView.findViewById(R.id.textViewNameFood);
            viewHolder.textViewPurchasePrice = convertView.findViewById(R.id.textViewPurchasePrice);
            viewHolder.cardViewFood= convertView.findViewById(R.id.cardViewFood);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.imageViewFood.setImageBitmap(convertToBitmap(foodModel.getImageFood()));
        viewHolder.textViewFoodName.setText(foodModel.getFoodName());
        viewHolder.textViewPurchasePrice.setText(foodModel.getSellingPrice());
        viewHolder.cardViewFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FoodModel foodModel1 = foodModelArrayList.get(position);
                Intent intent = new Intent(context, OrderFood.class);
//                intent.putExtra("food", new GsonBuilder().create().toJson(foodModel1));
                intent.putExtra("id", foodModel1.getId());
                intent.putExtra("name", foodModel1.getFoodName());
                intent.putExtra("quantity", foodModel1.getQuantity());
                intent.putExtra("selling_price", foodModel1.getSellingPrice());
                intent.putExtra("description", foodModel1.getDescription());

                Drawable drawable = viewHolder.imageViewFood.getDrawable();
                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] bytes = byteArrayOutputStream.toByteArray();
                intent.putExtra("image_food", bytes);

                context.startActivity(intent);
            }
        });
        return convertView;
    }

    private Bitmap convertToBitmap(byte[] imageFood) {
        return BitmapFactory.decodeByteArray(imageFood, 0, imageFood.length);
    }

    private static class ViewHolder {
        public ImageView imageViewFood;
        public CardView cardViewFood;
        public TextView textViewFoodName, textViewQuantity, textViewPurchasePrice, textViewDescription;
    }
}
