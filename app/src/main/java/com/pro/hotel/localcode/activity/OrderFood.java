package com.pro.hotel.localcode.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.pro.hotel.R;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.CartModel;
import com.pro.hotel.localcode.utils.Config;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OrderFood extends AppCompatActivity {

    Button buttonAddCount, buttonLessCount, buttonAddCart, buttonAddCart2;
    ImageView imageViewFood, imageViewBack;
    TextView textViewNameFood, textViewStatusQuantity, textViewSellingPrice, textViewDescription, textViewCount, textViewSumary;
    EditText editTextNote;
    Toolbar toolbar;

    SQLiteHelper sqLiteHelper;

    int count = 0, q, sum;
    String id_food, id_user, nameFood, qty, sellingPrice;
    SharedPreferences sharedPreferences;
    Bitmap bitmap1;

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_food);

        init();
        /*setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/

        sqLiteHelper = new SQLiteHelper(this);
        sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        id_user = sharedPreferences.getString(Config.ID_USER, "Not Available");

        final Intent intent = getIntent();
        id_food = intent.getStringExtra("id");
        nameFood = intent.getStringExtra("name");
        sellingPrice = intent.getStringExtra("selling_price");
        String desc = intent.getStringExtra("description");
        qty = intent.getStringExtra("quantity");
        q = Integer.parseInt(qty);
//        Log.d("Quantity ", qty);

        textViewNameFood.setText(nameFood);
        textViewSellingPrice.setText(sellingPrice);
        textViewDescription.setText(desc);
        textViewCount.setText(String.valueOf(count));

        if (q <= 0) {
            textViewStatusQuantity.setText("Not Available");
            updateFoodQuantity();
        } else {
            textViewStatusQuantity.setText("Available");
        }
        if (count == 0) {
//            buttonAddCart.setVisibility(View.GONE);
//            editTextNote.setVisibility(View.GONE);
        } else {
//            buttonAddCart.setVisibility(View.VISIBLE);
//            editTextNote.setVisibility(View.VISIBLE);
        }

        Bundle bundle = getIntent().getExtras();
        byte[] bytes = bundle.getByteArray("image_food");
        bitmap1 = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        imageViewFood.setImageBitmap(bitmap1);

        Drawable drawable = imageViewFood.getDrawable();
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        final byte[] bytes1 = byteArrayOutputStream.toByteArray();


        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonAddCart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(OrderFood.this, TotalPesanan.class);
                intent1.putExtra("image_food", bytes1);
                intent1.putExtra("harga", sellingPrice);
                intent1.putExtra("id", id_food);
                intent1.putExtra("name_food", nameFood);
                startActivity(intent1);
            }
        });

        buttonAddCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textViewStatusQuantity.getText().toString().equals("Available")) {
                    if (count == 10) {
                        count = 10;
                        textViewCount.setText(String.valueOf(count));
                    } else {
                        count = count + 1;
                        textViewCount.setText(String.valueOf(count));
                        sum = count * Integer.parseInt(textViewSellingPrice.getText().toString());
                        textViewSumary.setText(String.valueOf(sum));
                        if (q < count) {
                            editTextNote.setVisibility(View.GONE);
                            buttonAddCart.setVisibility(View.GONE);
                            count = q;
                            textViewCount.setText(String.valueOf(count));
                            sum = count * Integer.parseInt(textViewSellingPrice.getText().toString());
                            textViewSumary.setText(String.valueOf(sum));
                            Toast.makeText(OrderFood.this, "This item has " + q + " servings", Toast.LENGTH_SHORT).show();
                        } else {
                            if (count >= 0) {
//                                editTextNote.setVisibility(View.VISIBLE);
//                                buttonAddCart.setVisibility(View.VISIBLE);
                            } else {
//                                editTextNote.setVisibility(View.GONE);
//                                buttonAddCart.setVisibility(View.GONE);
                            }
//                            Log.d("Count ", String.valueOf(count));
                        }
                    }
                } else if (textViewStatusQuantity.getText().toString().equals("Not Available")) {
                    Toast.makeText(OrderFood.this, "Your Order is Not Available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        buttonLessCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    count = 0;
                    textViewCount.setText(String.valueOf(count));
                    editTextNote.setVisibility(View.GONE);
                    buttonAddCart.setVisibility(View.GONE);
                } else {
                    count = count - 1;
                    textViewCount.setText(String.valueOf(count));
                    sum = count * Integer.parseInt(textViewSellingPrice.getText().toString());
                    textViewSumary.setText(String.valueOf(sum));
                    if (count == 0) {
//                        editTextNote.setVisibility(View.GONE);
//                        buttonAddCart.setVisibility(View.GONE);
                    } else {
                        editTextNote.setVisibility(View.VISIBLE);
                        buttonAddCart.setVisibility(View.VISIBLE);
                    }
//                    Log.d("Count ", String.valueOf(count));

                }
            }
        });
        /*buttonAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int _count = Integer.parseInt(textViewCount.getText().toString());
//                Log.d("_Count ", String.valueOf(_count));

                int summ = Integer.parseInt(textViewSellingPrice.getText().toString());
//                Log.d("Selling ", String.valueOf(summ));

                int totalPrice = _count * summ;
//                Log.d("Total Price ", String.valueOf(totalPrice));

                String note = editTextNote.getText().toString();

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
                String strTime = simple.format(calendar.getTime());
                String date = strTime;

                String status = "Order";

                int _newQty = Integer.parseInt(qty) - _count;

                Drawable drawable = imageViewFood.getDrawable();
                bitmap1 = ((BitmapDrawable) drawable).getBitmap();

                File path = Environment.getExternalStorageDirectory();
                File dir = new File(path + "/CartImage/");
                dir.mkdir();

                String filename = String.format(textViewNameFood.getText().toString() + ".jpg", System.currentTimeMillis());
                File file = new File(dir, filename);

                *//*if (filename != null) {
                    Toast.makeText(this, "Adding is Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Adding is Failed", Toast.LENGTH_SHORT).show();
                    EmptyField();
                }*//*

                OutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(file);
                    bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte imageFood[] = byteArrayOutputStream.toByteArray();

                boolean isAdd = sqLiteHelper.addCart(new CartModel(null, id_user, id_food, nameFood, imageFood, _count, totalPrice, note, date, status));
                if (isAdd == true) {
                    Toast.makeText(OrderFood.this, "Add Cart Success", Toast.LENGTH_SHORT).show();
                    count = 0;
                    editTextNote.setVisibility(View.GONE);
                    buttonAddCart.setVisibility(View.GONE);

                    sqLiteHelper.updateFood(id_food, _newQty, date);
                } else {
                    Toast.makeText(OrderFood.this, "Add Cart is Failed!!!", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void updateFoodQuantity() {
        String qty = "0";

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        String strTime = simple.format(calendar.getTime());
        String date = strTime;

        sqLiteHelper.updateFoodQuantity(id_food, qty, date);
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        buttonAddCart2 = findViewById(R.id.buttonAddCart2);
        imageViewBack = findViewById(R.id.imageViewBack);
        buttonAddCount = findViewById(R.id.buttonAdd);
        buttonLessCount = findViewById(R.id.buttonLess);
        imageViewFood = findViewById(R.id.imageViewFood);
        textViewNameFood = findViewById(R.id.textViewNameFood);
        textViewStatusQuantity = findViewById(R.id.textViewQuantity);
        textViewSellingPrice = findViewById(R.id.textViewSellingPrice);
        textViewDescription = findViewById(R.id.textViewDescription);
        textViewCount = findViewById(R.id.textViewCount);
//        editTextNote = findViewById(R.id.editTextNote);
//        buttonAddCart = findViewById(R.id.buttonAddCart);
        textViewSumary = findViewById(R.id.textViewSumary);
    }
}
