package com.pro.hotel.localcode.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pro.hotel.R;
import com.pro.hotel.localcode.adapter.FoodAdapter;
import com.pro.hotel.localcode.adapter.UsersAdapter;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.FoodModel;
import com.pro.hotel.localcode.model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class UsersList extends AppCompatActivity {

    ListView listViewUsers;
    UserModel usersModel;
    EditText editTextSearch;
    List<UserModel> userModelList;
    UsersAdapter usersAdapter;
    SQLiteHelper sqLiteHelper;
    SwipeRefreshLayout swipeRefreshLayout;
    FloatingActionButton floatingActionButtonAdd;
    Toolbar toolbar;
    ImageView imageViewBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);

        init();

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        userModelList = new ArrayList<>();
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
        sqLiteHelper = new SQLiteHelper(this);

        showUserList();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                userModelList = new ArrayList<>(sqLiteHelper.getUsers());
                usersAdapter = new UsersAdapter(UsersList.this, (ArrayList<UserModel>) userModelList);
                listViewUsers.setAdapter(usersAdapter);
            }
        });
        floatingActionButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UsersList.this, SignUp.class);
                startActivity(intent);
            }
        });
    }

    private void showUserList() {
        final ArrayList<UserModel> userModelArrayList = new ArrayList<>(sqLiteHelper.getUsers());
        usersAdapter = new UsersAdapter(this, userModelArrayList);
        listViewUsers.setAdapter(usersAdapter);
    }

    private void filter(String name) {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        List<UserModel> userModelList = sqLiteHelper.searchUser(name);
        if (userModelList != null) {
            usersAdapter = new UsersAdapter(UsersList.this, (ArrayList<UserModel>) userModelList);
            listViewUsers.setAdapter(usersAdapter);
        }
    }

    private void init() {
        listViewUsers = findViewById(R.id.listViewUsers);
        toolbar = findViewById(R.id.toolbar);
        imageViewBack = findViewById(R.id.imageViewBack);
        editTextSearch = findViewById(R.id.editTextSearch);
        swipeRefreshLayout = findViewById(R.id.swipe);
        floatingActionButtonAdd = findViewById(R.id.fabAdd);
    }
}
