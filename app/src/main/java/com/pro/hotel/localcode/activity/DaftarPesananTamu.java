package com.pro.hotel.localcode.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;

import com.pro.hotel.R;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.HistoriModel;

import java.util.ArrayList;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.colorizers.TableDataRowColorizer;
import de.codecrafters.tableview.model.TableColumnDpWidthModel;
import de.codecrafters.tableview.providers.TableDataRowBackgroundProvider;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;
import de.codecrafters.tableview.toolkit.TableDataRowColorizers;

public class DaftarPesananTamu extends AppCompatActivity {

    TableView tableViewHistori;
    Toolbar toolbar;
    ImageView imageViewBack;

    private String[] tableHeaders = {"Nama", "No.Kamar", "Makanan", "Jumlah"};
    private String[][] tableData;
    SQLiteHelper sqLiteHelper;
    int colorEventRows, colorOddRows;
    TableColumnDpWidthModel tableColumnDpWidthModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pesanan_tamu);

        init();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sqLiteHelper = new SQLiteHelper(this);

        colorEventRows = getResources().getColor(R.color.colorPrimary);
        colorOddRows = getResources().getColor(R.color.colorAccent);

        tableColumnDpWidthModel = new TableColumnDpWidthModel(DaftarPesananTamu.this, 4, 100);
        tableColumnDpWidthModel.setColumnWidth(0, 200);//nama
        tableColumnDpWidthModel.setColumnWidth(1, 200);//no kamar
        tableColumnDpWidthModel.setColumnWidth(2, 200);//makanan
        tableColumnDpWidthModel.setColumnWidth(3, 200);//jumlah
        tableViewHistori.setColumnModel(tableColumnDpWidthModel);
        tableViewHistori.setHeaderBackgroundColor(getResources().getColor(R.color.colorWhite));
        tableViewHistori.setHeaderElevation(8);
        tableViewHistori.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(colorEventRows, colorOddRows));
        tableViewHistori.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHeaders));
        tableViewHistori.setDataAdapter(new SimpleTableDataAdapter(this, getHistory()));
    }

    private String[][] getHistory() {
        ArrayList<HistoriModel> janjangModelArrayList = (ArrayList<HistoriModel>)
                new SQLiteHelper(this)
                        .getHistory();
        HistoriModel historiModel;
        tableData = new String[janjangModelArrayList.size()][4];
        for (int i = 0; i < janjangModelArrayList.size(); i++) {
            historiModel = janjangModelArrayList.get(i);
            tableData[i][0] = historiModel.getNamaLengkap();
            tableData[i][1] = historiModel.getNomorKamar();
            tableData[i][2] = historiModel.getNamaMakanan();
            tableData[i][3] = historiModel.getJumlah();
        }
        return tableData;
    }

    private void init() {
        tableViewHistori = findViewById(R.id.table_data_view);
        toolbar = findViewById(R.id.toolbar);
        imageViewBack = findViewById(R.id.imageViewBack);
    }
}
