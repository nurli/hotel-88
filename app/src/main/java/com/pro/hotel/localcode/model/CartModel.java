package com.pro.hotel.localcode.model;

public class CartModel {
    public String id;
    public String _idUser;
    public String _idFood;
    public String nameFood;
    public byte[] imageFood;
    public int count;
    public int totalPrice;
    public String note;
    public String create_at;
    public String status;

    public CartModel() {

    }

    public CartModel(String id, String id_user, String id_food, String nameFood, byte[] imageFood, int count, int totalPrice, String note, String create_at, String status) {
        this.id = id;
        this._idUser = id_user;
        this._idFood = id_food;
        this.nameFood = nameFood;
        this.imageFood = imageFood;
        this.count = count;
        this.totalPrice = totalPrice;
        this.note = note;
        this.create_at = create_at;
        this.status = status;
    }

    public byte[] getImageFood() {
        return imageFood;
    }

    public void setImageFood(byte[] imageFood) {
        this.imageFood = imageFood;
    }

    public String getNameFood() {
        return nameFood;
    }

    public void setNameFood(String nameFood) {
        this.nameFood = nameFood;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get_idUser() {
        return _idUser;
    }

    public void set_idUser(String _idUser) {
        this._idUser = _idUser;
    }

    public String get_idFood() {
        return _idFood;
    }

    public void set_idFood(String _idFood) {
        this._idFood = _idFood;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
