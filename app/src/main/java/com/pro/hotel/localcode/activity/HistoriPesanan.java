package com.pro.hotel.localcode.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.pro.hotel.R;
import com.pro.hotel.localcode.adapter.CartAdapter;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.CartModel;
import com.pro.hotel.localcode.model.HistoriModel;

import java.util.ArrayList;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import de.codecrafters.tableview.model.TableColumnDpWidthModel;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;

public class HistoriPesanan extends AppCompatActivity {

    TableView tableViewHistori;
    Toolbar toolbar;
    ImageView imageViewBack;

    private String[] tableHeaders = {"Nama", "Makanan", "Jumlah", "Total", "Tanggal"};
    private String[][] tableData;
    SQLiteHelper sqLiteHelper;
    int colorEventRows, colorOddRows;
    TableColumnDpWidthModel tableColumnDpWidthModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histori_pesanan);

        init();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        sqLiteHelper = new SQLiteHelper(this);

        colorEventRows = getResources().getColor(R.color.colorWhite);
        colorOddRows = getResources().getColor(R.color.colorAccent);

        tableColumnDpWidthModel = new TableColumnDpWidthModel(HistoriPesanan.this, 5, 100);
        tableColumnDpWidthModel.setColumnWidth(0, 150);//nama
        tableColumnDpWidthModel.setColumnWidth(1, 150);//makanan
        tableColumnDpWidthModel.setColumnWidth(2, 150);//jumlah
        tableColumnDpWidthModel.setColumnWidth(3, 150);//total
        tableColumnDpWidthModel.setColumnWidth(4, 150);//tanggal
        tableViewHistori.setColumnModel(tableColumnDpWidthModel);
        tableViewHistori.setHeaderBackgroundColor(getResources().getColor(R.color.colorWhite));
        tableViewHistori.setHeaderElevation(8);
        tableViewHistori.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(colorEventRows, colorOddRows));
        tableViewHistori.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHeaders));
        tableViewHistori.setDataAdapter(new SimpleTableDataAdapter(this, getHistory()));
    }

    private String[][] getHistory() {
        ArrayList<HistoriModel> janjangModelArrayList = (ArrayList<HistoriModel>)
                new SQLiteHelper(this)
                        .getHistory();
        HistoriModel historiModel;
        tableData = new String[janjangModelArrayList.size()][5];
        for (int i = 0; i < janjangModelArrayList.size(); i++) {
            historiModel = janjangModelArrayList.get(i);
            tableData[i][0] = historiModel.getNamaLengkap();
            tableData[i][1] = historiModel.getNamaMakanan();
            tableData[i][2] = historiModel.getJumlah();
            tableData[i][3] = historiModel.getTotal();
            tableData[i][4] = historiModel.getTanggal();
        }
        return tableData;
    }

    private void init() {
        tableViewHistori = findViewById(R.id.table_data_view);
        toolbar = findViewById(R.id.toolbar);
        imageViewBack = findViewById(R.id.imageViewBack);
    }
}
