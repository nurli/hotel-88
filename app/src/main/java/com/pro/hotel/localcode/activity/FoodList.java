package com.pro.hotel.localcode.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pro.hotel.R;
import com.pro.hotel.localcode.adapter.FoodAdapter;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.FoodModel;

import java.util.ArrayList;
import java.util.List;

public class FoodList extends AppCompatActivity {

    FloatingActionButton floatingActionButtonAdd;
    ListView listViewFood;
    FoodModel foodModel;
    EditText editTextSearch;
    List<FoodModel> foodModelList;
    FoodAdapter foodAdapter;
    SQLiteHelper sqLiteHelper;
    SwipeRefreshLayout swipeRefreshLayout;
    Toolbar toolbar;
    ImageView imageViewBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        init();

        foodModelList = new ArrayList<>();
        sqLiteHelper = new SQLiteHelper(this);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        showFoodList();

        floatingActionButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FoodList.this, AddFood.class);
                startActivity(intent);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                foodModelList = new ArrayList<>(sqLiteHelper.getFoods());
                foodAdapter = new FoodAdapter(FoodList.this, (ArrayList<FoodModel>) foodModelList);
                listViewFood.setAdapter(foodAdapter);
            }
        });
    }

    private void showFoodList() {
        final ArrayList<FoodModel> foodModels = new ArrayList<>(sqLiteHelper.getFoods());
        foodAdapter = new FoodAdapter(this, foodModels);
        listViewFood.setAdapter(foodAdapter);
    }

    private void filter(String name) {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        List<FoodModel> foodModelList = sqLiteHelper.searchFood(name);
        if (foodModelList != null) {
            foodAdapter = new FoodAdapter(FoodList.this, (ArrayList<FoodModel>) foodModelList);
            listViewFood.setAdapter(foodAdapter);
        }
    }

    @Override
    protected void onResume() {
        final ArrayList<FoodModel> foodModels = new ArrayList<FoodModel>(sqLiteHelper.getFoods());
        foodAdapter = new FoodAdapter(FoodList.this, foodModels);
        listViewFood.setAdapter(foodAdapter);
        super.onResume();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        imageViewBack = findViewById(R.id.imageViewBack);
        floatingActionButtonAdd = findViewById(R.id.fabAdd);
        listViewFood = findViewById(R.id.listViewFood);
        editTextSearch = findViewById(R.id.editTextSearch);
        swipeRefreshLayout = findViewById(R.id.swipe);
    }
}
