package com.pro.hotel.localcode.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

import com.pro.hotel.R;
import com.pro.hotel.localcode.adapter.UserFoodAdapter;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.FoodModel;
import com.pro.hotel.localcode.utils.Config;

import java.util.ArrayList;
import java.util.List;

public class BuatPesanan extends AppCompatActivity {

    ListView listViewFood;
    FoodModel foodModel;
    EditText editTextSearch;
    List<FoodModel> foodModelList;
    UserFoodAdapter userFoodAdapter;
    SQLiteHelper sqLiteHelper;
    ImageView imageViewBack;
//    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user);

        init();

        foodModelList = new ArrayList<>();

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
        sqLiteHelper = new SQLiteHelper(this);

        showFoodList();

        /*swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                foodModelList = new ArrayList<>(sqLiteHelper.getFood());
                userFoodAdapter = new UserFoodAdapter(BuatPesanan.this, (ArrayList<FoodModel>) foodModelList);
                listViewFood.setAdapter(userFoodAdapter);
            }
        });*/
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showFoodList() {
        final ArrayList<FoodModel> foodModels = new ArrayList<>(sqLiteHelper.getFood());
        userFoodAdapter = new UserFoodAdapter(this, foodModels);
        listViewFood.setAdapter(userFoodAdapter);
    }

    private void filter(String name) {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        List<FoodModel> foodModelList = sqLiteHelper.searchFood(name);
        if (foodModelList != null) {
            userFoodAdapter = new UserFoodAdapter(BuatPesanan.this, (ArrayList<FoodModel>) foodModelList);
            listViewFood.setAdapter(userFoodAdapter);
        }
    }

    private void init() {
        imageViewBack = findViewById(R.id.imageViewBack);
        listViewFood = findViewById(R.id.listViewFood);
        editTextSearch = findViewById(R.id.editTextSearch);
//        swipeRefreshLayout = findViewById(R.id.swipe);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_main_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logOut:
                LogOutProgress();
                break;
            case R.id.cart:
                Intent intent = new Intent(BuatPesanan.this, MyCart.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void LogOutProgress() {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(this);
        alert.setTitle("Log Out");
        alert.setMessage("Are you sure you want to Log Out?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);
                                editor.commit();
                                Intent intent = new Intent(BuatPesanan.this, Login.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
        androidx.appcompat.app.AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }
}
