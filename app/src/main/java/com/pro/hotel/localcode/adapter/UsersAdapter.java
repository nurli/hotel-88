package com.pro.hotel.localcode.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pro.hotel.R;
import com.pro.hotel.localcode.model.UserModel;

import java.util.ArrayList;

public class UsersAdapter extends ArrayAdapter<UserModel> {
    Context context;
    private ArrayList<UserModel> userModelArrayList;

    public UsersAdapter(Context context, ArrayList<UserModel> userModelArrayList){
        super(context, R.layout.list_view_users, userModelArrayList);
        this.context = context;
        this.userModelArrayList= userModelArrayList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final UserModel userModel = getItem(position);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = ((Activity) context).getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.list_view_users, parent, false);
            viewHolder.textViewUsername = convertView.findViewById(R.id.textViewUsername);
            viewHolder.textViewPhoneNumber = convertView.findViewById(R.id.textViewPhoneNumber);
            viewHolder.textViewAgama = convertView.findViewById(R.id.textViewAgama);
            viewHolder.textViewAlamat = convertView.findViewById(R.id.textViewAlamat);
            viewHolder.textViewTempatLahir = convertView.findViewById(R.id.textViewTempatLahir);
            viewHolder.textViewTanggalLahir = convertView.findViewById(R.id.textViewTanggalLahir);
            viewHolder.textViewJenisKelamin= convertView.findViewById(R.id.textViewJenisKelamin);
            viewHolder.textViewNoKamar = convertView.findViewById(R.id.textViewNoKamar);
            viewHolder.textViewLevel = convertView.findViewById(R.id.textViewLevel);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textViewUsername.setText("Username : " + userModel.getUsername());
        viewHolder.textViewPhoneNumber.setText("Phone Number : " + userModel.getPhoneNumber());
        viewHolder.textViewNoKamar.setText("No Kamar : " + userModel.getNoKamar());
        viewHolder.textViewJenisKelamin.setText("Jenis Kelamin: " + userModel.getJnsKelamin());
        viewHolder.textViewTempatLahir.setText("Tempat Lahir : " + userModel.getTempatLahir());
        viewHolder.textViewTanggalLahir.setText("Tangal Lahir : " + userModel.getTanggalLahir());
        viewHolder.textViewAgama.setText("Agama : " + userModel.getAgama());
        viewHolder.textViewAlamat.setText("Agama : " + userModel.getAddress());
        viewHolder.textViewLevel.setText("Level : " + userModel.getLevel());
        return convertView;

    }

    private static class ViewHolder{
        public TextView textViewUsername, textViewPhoneNumber, textViewAgama, textViewAlamat, textViewJenisKelamin, textViewLevel, textViewNoKamar, textViewTempatLahir, textViewTanggalLahir;
    }
}
