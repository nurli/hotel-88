package com.pro.hotel.localcode.model;

public class UserModel {
    public String id;
    public String username;
    public String noKamar;
    public String jnsKelamin;
    public String tempatLahir;
    public String tanggalLahir;
    public String phoneNumber;
    public String agama;
    public String address;
    public String level;

    public UserModel() {

    }

    public UserModel(String id, String username, String noKamar, String jnsKelamin, String tempatLahir, String tanggalLahir, String phoneNumber, String agama, String address, String level){
        this.id = id;
        this.username = username;
        this.noKamar = noKamar;
        this.jnsKelamin = jnsKelamin;
        this.tempatLahir = tempatLahir;
        this.tanggalLahir= tanggalLahir;
        this.phoneNumber = phoneNumber;
        this.agama = agama;
        this.address = address;
        this.level = level;
    }

    public UserModel(String id, String username, String noKamar){
        this.id = id;
        this.username = username;
        this.noKamar = noKamar;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNoKamar() {
        return noKamar;
    }

    public void setNoKamar(String noKamar) {
        this.noKamar = noKamar;
    }

    public String getJnsKelamin() {
        return jnsKelamin;
    }

    public void setJnsKelamin(String jnsKelamin) {
        this.jnsKelamin = jnsKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
