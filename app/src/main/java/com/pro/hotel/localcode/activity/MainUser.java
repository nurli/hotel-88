package com.pro.hotel.localcode.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.pro.hotel.R;
import com.pro.hotel.localcode.utils.Config;

public class MainUser extends AppCompatActivity {

    Button buttonBuatPesanan, buttonHistoriPesanan, getButtonHistoriPembayaran;
    TextView textViewNamaUser;
    ImageView imageViewLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user2);

        init();

        imageViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogOutProgress();
            }
        });
        buttonBuatPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainUser.this, BuatPesanan.class);
                startActivity(intent);
            }
        });
        buttonHistoriPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainUser.this, HistoriPesanan.class);
                startActivity(intent);
            }
        });
        getButtonHistoriPembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainUser.this, HistoriPembayaran.class);
                startActivity(intent);
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String user = sharedPreferences.getString(Config.USERNAME, "Not Available");
        textViewNamaUser.setText(user);
    }

    private void LogOutProgress() {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(this);
        alert.setTitle("Log Out");
        alert.setMessage("Are you sure you want to Log Out?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);
                                editor.commit();
                                Intent intent = new Intent(MainUser.this, Login.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
        androidx.appcompat.app.AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    private void init() {
        getButtonHistoriPembayaran = findViewById(R.id.buttonHistoriPembayaran);
        buttonBuatPesanan = findViewById(R.id.buttonBuatPesanan);
        buttonHistoriPesanan = findViewById(R.id.buttonHistoriPesanan);
        textViewNamaUser = findViewById(R.id.textViewNAmaUser);
        imageViewLogout = findViewById(R.id.imageViewLogOut);
    }
}
