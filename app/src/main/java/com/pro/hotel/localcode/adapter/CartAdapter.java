package com.pro.hotel.localcode.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityRecord;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.pro.hotel.R;
import com.pro.hotel.localcode.activity.DetailFood;
import com.pro.hotel.localcode.activity.MyCart;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.CartModel;

import java.util.ArrayList;

public class CartAdapter extends ArrayAdapter<CartModel> {
    Context context;
    private ArrayList<CartModel> cartModels;
    SQLiteHelper sqLiteHelper;
    MyCart myCart;

    public CartAdapter(Context context, ArrayList<CartModel> cartModelArrayList){
        super(context, R.layout.list_view_cart, cartModelArrayList);
        this.context = context;
        this.cartModels = cartModelArrayList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CartModel cartModel = getItem(position);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = ((Activity) context).getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.list_view_cart, parent, false);
            viewHolder.imageViewFood = convertView.findViewById(R.id.imageViewFood);
            viewHolder.textViewFoodName = convertView.findViewById(R.id.textViewNameFood);
            viewHolder.textViewCount = convertView.findViewById(R.id.textViewCount);
            viewHolder.textViewTotalPrice = convertView.findViewById(R.id.textViewTotalPrice);
            viewHolder.textViewStatus = convertView.findViewById(R.id.textViewStatus);
            viewHolder.textViewDate = convertView.findViewById(R.id.textViewDate);
            viewHolder.textViewNote = convertView.findViewById(R.id.textViewNote);
            viewHolder.imageViewDelete = convertView.findViewById(R.id.imageViewDelete);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.imageViewFood.setImageBitmap(convertToBitmap(cartModel.getImageFood()));
        viewHolder.textViewFoodName.setText(cartModel.getNameFood());
        viewHolder.textViewCount.setText(String.valueOf(cartModel.getCount()));
        viewHolder.textViewTotalPrice.setText(String.valueOf(cartModel.getTotalPrice()));
        viewHolder.textViewStatus.setText(cartModel.getStatus());
        viewHolder.textViewDate.setText(cartModel.getCreate_at());
        viewHolder.textViewNote.setText(cartModel.getNote());
        viewHolder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartModel cartModel1 = getItem(position);
                final String id = cartModel1.getId();
                sqLiteHelper = new SQLiteHelper(context);
                myCart = new MyCart();
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Delete");
                alert.setMessage("Are you sure you want to Delete this Item?")
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sqLiteHelper.removeMyCart(id);
                                        Toast.makeText(context, "Delete Item Success", Toast.LENGTH_SHORT).show();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });

        return convertView;
    }

    private Bitmap convertToBitmap(byte[] imageFood) {
        return BitmapFactory.decodeByteArray(imageFood, 0, imageFood.length);
    }

    private static class ViewHolder {
        public ImageView imageViewFood, imageViewDelete;
        public CardView cardViewFood;
        public TextView textViewFoodName, textViewCount, textViewTotalPrice, textViewStatus, textViewDate, textViewNote;
    }
}
