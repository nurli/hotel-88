package com.pro.hotel.localcode.model;

import com.pro.hotel.localcode.activity.HistoriPesanan;

public class HistoriModel {

    public String id;
    public String namaLengkap;
    public String namaMakanan;
    public String nomorKamar;
    public String jumlah;
    public String total;
    public String tanggal;

    public HistoriModel(){

    }

    public HistoriModel(String id, String namaLengkap, String namaMakanan, String nomorKamar, String jumlah, String total, String tanggal) {
        this.id = id;
        this.namaLengkap = namaLengkap;
        this.namaMakanan = namaMakanan;
        this.nomorKamar = nomorKamar;
        this.jumlah = jumlah;
        this.total = total;
        this.tanggal = tanggal;
    }

    public String getNomorKamar() {
        return nomorKamar;
    }

    public void setNomorKamar(String nomorKamar) {
        this.nomorKamar = nomorKamar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getNamaMakanan() {
        return namaMakanan;
    }

    public void setNamaMakanan(String namaMakanan) {
        this.namaMakanan = namaMakanan;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
