package com.pro.hotel.localcode.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pro.hotel.localcode.model.CartModel;
import com.pro.hotel.localcode.model.FoodModel;
import com.pro.hotel.localcode.model.HistoriModel;
import com.pro.hotel.localcode.model.UserModel;

import java.util.ArrayList;
import java.util.List;


public class SQLiteHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "hotel88.db";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_USER = "user";
    public static final String KEY_USER_ID = "id_user";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_ROOM_NUMBER = "no_kamar";
    public static final String KEY_JNS_KELAMIN = "jns_kelamin";
    public static final String KEY_TEMPAT_LAHIR = "tempat_lahir";
    public static final String KEY_TANGGAL_LAHIR = "tanggal_lahir";
    public static final String KEY_PHONE_NUMBER = "phone_number";
    public static final String KEY_RELIGION = "religion";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_EMAIL_USER = "email_user";
    public static final String KEY_PASSWORD_USER = "password_user";
    public static final String KEY_LEVEL_USER = "level_user";

    public static final String SQL_TABLE_USER = "CREATE TABLE " + TABLE_USER +
            "( " + KEY_USER_ID + " integer primary key," +
            KEY_USERNAME + " text," +
            KEY_ROOM_NUMBER + " text," +
            KEY_JNS_KELAMIN + " text," +
            KEY_TEMPAT_LAHIR + " text," +
            KEY_TANGGAL_LAHIR + " text," +
            KEY_PHONE_NUMBER + " text," +
            KEY_RELIGION + " text," +
            KEY_ADDRESS + " text," +
            KEY_LEVEL_USER + " text" +
            " )";

    public static final String TABLE_FOOD = "food";
    public static final String KEY_FOOD_ID = "id_food";
    public static final String KEY_FOODNAME = "foodname";
    public static final String KEY_QUANTITY = "quantity";
    public static final String KEY_PURCHASE_PRICE = "purchase_price";
    public static final String KEY_SELLING_PRICE = "selling_price";
    public static final String KEY_IMAGE_FOOD = "image_food";
    public static final String KEY_DATE_FOOD = "date";
    public static final String KEY_DESCRIPTION = "description";

    public static final String SQL_TABLE_FOOD = "CREATE TABLE " + TABLE_FOOD +
            "( " + KEY_FOOD_ID + " integer primary key," +
            KEY_FOODNAME + " text," +
            KEY_QUANTITY + " text," +
            KEY_PURCHASE_PRICE + " text," +
            KEY_SELLING_PRICE + " text," +
            KEY_IMAGE_FOOD + " blob," +
            KEY_DATE_FOOD + " text," +
            KEY_DESCRIPTION + " text" +
            " )";

    public static final String TABLE_CART = "cart";
    public static final String KEY_CART_ID = "id";
    public static final String KEY_ID_USER = "id_user";
    public static final String KEY_ID_FOOD = "id_food";
    public static final String KEY_NAME_FOOD = "name_food";
    public static final String KEY_FOOD_IMAGE = "image_food";
    public static final String KEY_COUNT = "count";
    public static final String KEY_TOTAL_PRICE = "total_price";
    public static final String KEY_NOTE = "note";
    public static final String KEY_CREATE_AT = "crate_at";
    public static final String KEY_STATUS = "status";

    public static final String SQL_TABLE_CART = "CREATE TABLE " + TABLE_CART +
            "( " + KEY_CART_ID + " integer primary key," +
            KEY_ID_USER + " text," +
            KEY_ID_FOOD + " text," +
            KEY_NAME_FOOD + " text," +
            KEY_FOOD_IMAGE + " blob," +
            KEY_COUNT + " integer," +
            KEY_TOTAL_PRICE + " integer," +
            KEY_NOTE + " text," +
            KEY_CREATE_AT + " text," +
            KEY_STATUS + " text" +
            " )";

    public static final String TABLE_HISTORY = "history";
    public static final String KEY_HISTORY_ID = "id";
    public static final String KEY_NAME_USER = "userName";
    public static final String KEY_NAME_FOOD_HISTORY = "foddName";
    public static final String KEY_ROOM_NUMBER_HISTORY = "roomNumber";
    public static final String KEY_COUNT_HISTORY = "countHistory";
    public static final String KEY_TOTAL_HISTORY = "totalHistory";
    public static final String KEY_DATE = "date";

    public static final String SQL_TABLE_HISTORY = "CREATE TABLE " + TABLE_HISTORY +
            "( " + KEY_HISTORY_ID + " integer primary key," +
            KEY_NAME_USER + " text," +
            KEY_NAME_FOOD_HISTORY + " text," +
            KEY_ROOM_NUMBER_HISTORY + " text," +
            KEY_COUNT_HISTORY + " text," +
            KEY_TOTAL_HISTORY + " text," +
            KEY_DATE + " text" +
            " )";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_TABLE_HISTORY);
        db.execSQL(SQL_TABLE_CART);
        db.execSQL(SQL_TABLE_FOOD);
        db.execSQL(SQL_TABLE_USER);
        db.execSQL("INSERT INTO " + TABLE_USER + " VALUES(1,'admin','000','Laki-laki','Jakarta','07/02/1982','082299398699','Islam','Jl. Kenanga Indah','1')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOOD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        onCreate(db);
    }

    public void AddFood(FoodModel foodModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FOOD_ID, foodModel.getId());
        contentValues.put(KEY_FOODNAME, foodModel.getFoodName());
        contentValues.put(KEY_QUANTITY, foodModel.getQuantity());
        contentValues.put(KEY_PURCHASE_PRICE, foodModel.getPurchasePrice());
        contentValues.put(KEY_SELLING_PRICE, foodModel.getSellingPrice());
        contentValues.put(KEY_IMAGE_FOOD, foodModel.getImageFood());
        contentValues.put(KEY_DATE_FOOD, foodModel.getDate());
        contentValues.put(KEY_DESCRIPTION, foodModel.getDescription());

        database.insert(TABLE_FOOD, null, contentValues);
        database.close();
    }

    public boolean isFoodExists(String mandoruser) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_FOOD,// Selecting Table
                new String[]{KEY_FOOD_ID, KEY_FOODNAME, KEY_QUANTITY, KEY_PURCHASE_PRICE, KEY_SELLING_PRICE, KEY_IMAGE_FOOD, KEY_DATE_FOOD, KEY_DESCRIPTION},//Selecting columns want to query
                KEY_FOODNAME + "=?",
                new String[]{mandoruser},//Where clause
                null, null, null);

        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            //if cursor has value then in user database there is user associated with this given email so return true
            return true;
        }
        //if email does not exist return false
        return false;
    }

    public List<FoodModel> searchFood(String key) {
        List<FoodModel> mandorModels = new ArrayList<FoodModel>();
        try {
            SQLiteDatabase database = getWritableDatabase();
            Cursor cursor = database.rawQuery("select * from " + TABLE_FOOD + " where " + KEY_FOODNAME + " like ?",
                    new String[]{"%" + key + "%"});
            if (cursor.moveToFirst()) {
                do {
                    FoodModel foodModel = new FoodModel();
                    foodModel.setId(cursor.getString(0));
                    foodModel.setFoodName(cursor.getString(1));
                    foodModel.setQuantity(cursor.getString(2));
                    foodModel.setPurchasePrice(cursor.getString(3));
                    foodModel.setSellingPrice(cursor.getString(4));
                    foodModel.setImageFood(cursor.getBlob(5));
                    foodModel.setDate(cursor.getString(6));
                    foodModel.setDescription(cursor.getString(7));
                    mandorModels.add(foodModel);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            mandorModels = null;
        }
        return mandorModels;
    }

    public List<CartModel> searchMyCart(String key) {
        List<CartModel> cartModels = new ArrayList<CartModel>();
        try {
            SQLiteDatabase database = getWritableDatabase();
            Cursor cursor = database.rawQuery("select * from " + TABLE_CART + " where " + KEY_NAME_FOOD + " like ?",
                    new String[]{"%" + key + "%"});
            if (cursor.moveToFirst()) {
                do {
                    CartModel cartModel = new CartModel();
                    cartModel.setId(cursor.getString(0));
                    cartModel.set_idUser(cursor.getString(1));
                    cartModel.set_idFood(cursor.getString(2));
                    cartModel.setNameFood(cursor.getString(3));
                    cartModel.setImageFood(cursor.getBlob(4));
                    cartModel.setCount(cursor.getInt(5));
                    cartModel.setTotalPrice(cursor.getInt(6));
                    cartModel.setNote(cursor.getString(7));
                    cartModel.setCreate_at(cursor.getString(8));
                    cartModel.setStatus(cursor.getString(9));
                    cartModels.add(cartModel);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            cartModels = null;
        }
        return cartModels;
    }

    public List<UserModel> searchUser(String key) {
        List<UserModel> userModels = new ArrayList<UserModel>();
        try {
            SQLiteDatabase database = getWritableDatabase();
            Cursor cursor = database.rawQuery("select * from " + TABLE_USER + " where " + KEY_USERNAME + " like ?",
                    new String[]{"%" + key + "%"});
            if (cursor.moveToFirst()) {
                do {
                    UserModel userModel = new UserModel();
                    userModel.setId(cursor.getString(0));
                    userModel.setUsername(cursor.getString(1));
                    userModel.setNoKamar(cursor.getString(2));
                    userModel.setJnsKelamin(cursor.getString(3));
                    userModel.setTempatLahir(cursor.getString(4));
                    userModel.setTanggalLahir(cursor.getString(5));
                    userModel.setPhoneNumber(cursor.getString(6));
                    userModel.setAgama(cursor.getString(7));
                    userModel.setAddress(cursor.getString(8));
                    userModel.setLevel(cursor.getString(9));
                    userModels.add(userModel);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            userModels = null;
        }
        return userModels;
    }

    public void AddUser(UserModel userModel) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_USER_ID, userModel.getId());
        contentValues.put(KEY_USERNAME, userModel.getUsername());
        contentValues.put(KEY_ROOM_NUMBER, userModel.getNoKamar());
        contentValues.put(KEY_JNS_KELAMIN, userModel.getJnsKelamin());
        contentValues.put(KEY_TEMPAT_LAHIR, userModel.getTempatLahir());
        contentValues.put(KEY_TANGGAL_LAHIR, userModel.getTanggalLahir());
        contentValues.put(KEY_PHONE_NUMBER, userModel.getPhoneNumber());
        contentValues.put(KEY_RELIGION, userModel.getAgama());
        contentValues.put(KEY_ADDRESS, userModel.getAddress());
        contentValues.put(KEY_LEVEL_USER, userModel.getLevel());

        sqLiteDatabase.insert(TABLE_USER, null, contentValues);
        sqLiteDatabase.close();
    }

    public boolean isUserExists(String nama) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER,// Selecting Table
                new String[]{KEY_USER_ID, KEY_USERNAME, KEY_ROOM_NUMBER, KEY_JNS_KELAMIN, KEY_TEMPAT_LAHIR, KEY_TANGGAL_LAHIR, KEY_PHONE_NUMBER, KEY_RELIGION, KEY_ADDRESS, KEY_LEVEL_USER},//Selecting columns want to query
                KEY_USERNAME + "=?",
                new String[]{nama},//Where clause
                null, null, null);

        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            //if cursor has value then in user database there is user associated with this given email so return true
            return true;
        }

        //if email does not exist return false
        return false;
    }

    public UserModel Authenticate(UserModel userModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_USER,
                new String[]{
                        KEY_USER_ID, KEY_USERNAME, KEY_ROOM_NUMBER, KEY_JNS_KELAMIN, KEY_TEMPAT_LAHIR, KEY_TANGGAL_LAHIR, KEY_PHONE_NUMBER, KEY_RELIGION, KEY_ADDRESS, KEY_LEVEL_USER
                },
                KEY_USERNAME + "=?",
                new String[]{userModel.getUsername()},
                null, null, null);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            UserModel userModel1 = new UserModel(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8),
                    cursor.getString(9));
            if (userModel.getNoKamar().equalsIgnoreCase(userModel1.getNoKamar())) {
                return userModel1;
            }
        }
        return null;
    }

    public List<FoodModel> getFoods() {
        List<FoodModel> foodModelList = new ArrayList<FoodModel>();
        String query = "SELECT * FROM " + TABLE_FOOD;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                FoodModel foodModel = new FoodModel();
                foodModel.setId(cursor.getString(0));
                foodModel.setFoodName(cursor.getString(1));
                foodModel.setQuantity(cursor.getString(2));
                foodModel.setPurchasePrice(cursor.getString(3));
                foodModel.setSellingPrice(cursor.getString(4));
                foodModel.setImageFood(cursor.getBlob(5));
                foodModel.setDate(cursor.getString(6));
                foodModel.setDescription(cursor.getString(7));
                foodModelList.add(foodModel);
            } while (cursor.moveToNext());
        }
        return foodModelList;
    }

    public List<FoodModel> getFood() {
        List<FoodModel> foodModelList = new ArrayList<FoodModel>();
        String query = "SELECT * FROM " + TABLE_FOOD;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                FoodModel foodModel = new FoodModel();
                foodModel.setId(cursor.getString(0));
                foodModel.setFoodName(cursor.getString(1));
                foodModel.setQuantity(cursor.getString(2));
                foodModel.setPurchasePrice(cursor.getString(3));
                foodModel.setSellingPrice(cursor.getString(4));
                foodModel.setImageFood(cursor.getBlob(5));
                foodModel.setDate(cursor.getString(6));
                foodModel.setDescription(cursor.getString(7));
                foodModelList.add(foodModel);
            } while (cursor.moveToNext());
        }
        return foodModelList;
    }

    public List<CartModel> getCart() {
        List<CartModel> cartModelList = new ArrayList<CartModel>();
        String query = "SELECT * FROM " + TABLE_CART;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                CartModel cartModel = new CartModel();
                cartModel.setId(cursor.getString(0));
                cartModel.set_idUser(cursor.getString(1));
                cartModel.set_idFood(cursor.getString(2));
                cartModel.setNameFood(cursor.getString(3));
                cartModel.setImageFood(cursor.getBlob(4));
                cartModel.setCount(cursor.getInt(5));
                cartModel.setTotalPrice(cursor.getInt(6));
                cartModel.setNote(cursor.getString(7));
                cartModel.setCreate_at(cursor.getString(8));
                cartModel.setStatus(cursor.getString(9));
                cartModelList.add(cartModel);
            } while (cursor.moveToNext());
        }
        return cartModelList;
    }

    public List<HistoriModel> getHistory() {
        List<HistoriModel> historiModelList = new ArrayList<HistoriModel>();
        String query = "SELECT * FROM " + TABLE_HISTORY;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                HistoriModel historiModel = new HistoriModel();
                historiModel.setId(cursor.getString(0));
                historiModel.setNamaLengkap(cursor.getString(1));
                historiModel.setNamaMakanan(cursor.getString(2));
                historiModel.setNomorKamar(cursor.getString(3));
                historiModel.setJumlah(cursor.getString(4));
                historiModel.setTotal(cursor.getString(5));
                historiModel.setTanggal(cursor.getString(6));
                historiModelList.add(historiModel);
            } while (cursor.moveToNext());
        }
        return historiModelList;
    }

    public List<UserModel> getUsers() {
        List<UserModel> userModelList = new ArrayList<UserModel>();
        String query = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                UserModel userModel = new UserModel();
                userModel.setId(cursor.getString(0));
                userModel.setUsername(cursor.getString(1));
                userModel.setNoKamar(cursor.getString(2));
                userModel.setJnsKelamin(cursor.getString(3));
                userModel.setTempatLahir(cursor.getString(4));
                userModel.setTanggalLahir(cursor.getString(5));
                userModel.setPhoneNumber(cursor.getString(6));
                userModel.setAgama(cursor.getString(7));
                userModel.setAddress(cursor.getString(8));
                userModel.setLevel(cursor.getString(9));
                userModelList.add(userModel);
            } while (cursor.moveToNext());
        }
        return userModelList;
    }

    public void EditFood(String id, String nameFood, String qty, String purchasePrice, String sellingPrice, byte[] imageFood, String date) {
        SQLiteDatabase database = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_FOOD + " SET "
                + KEY_FOODNAME + "='" + nameFood + "'"
                + KEY_QUANTITY + "='" + qty + "'"
                + KEY_PURCHASE_PRICE + "='" + purchasePrice + "'"
                + KEY_SELLING_PRICE + "='" + sellingPrice + "'"
                + KEY_IMAGE_FOOD + "='" + imageFood + "'"
                + KEY_DATE_FOOD + "='" + date + "'"
                + " WHERE " + KEY_FOOD_ID + "=" + "'" + id + "'";
        database.execSQL(query);
        database.close();
    }

    public boolean EditFoodd(String id, String nameFood, String qty, String purchasePrice, String sellingPrice, byte[] imageFood, String date, String desc) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FOODNAME, nameFood);
        contentValues.put(KEY_QUANTITY, qty);
        contentValues.put(KEY_PURCHASE_PRICE, purchasePrice);
        contentValues.put(KEY_SELLING_PRICE, sellingPrice);
        contentValues.put(KEY_IMAGE_FOOD, imageFood);
        contentValues.put(KEY_DATE_FOOD, date);
        contentValues.put(KEY_DESCRIPTION, desc);
        sqLiteDatabase.update(TABLE_FOOD, contentValues, KEY_FOOD_ID + "= ?", new String[]{id});
        return true;
    }

    public void deleteFood(String id) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("delete from " + TABLE_FOOD + " where " + id + "=" + KEY_FOOD_ID);
        database.close();
    }

    public boolean addCart(CartModel cartModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CART_ID, cartModel.getId());
        contentValues.put(KEY_USER_ID, cartModel.get_idUser());
        contentValues.put(KEY_FOOD_ID, cartModel.get_idFood());
        contentValues.put(KEY_NAME_FOOD, cartModel.getNameFood());
        contentValues.put(KEY_IMAGE_FOOD, cartModel.getImageFood());
        contentValues.put(KEY_COUNT, cartModel.getCount());
        contentValues.put(KEY_TOTAL_PRICE, cartModel.getTotalPrice());
        contentValues.put(KEY_NOTE, cartModel.getNote());
        contentValues.put(KEY_CREATE_AT, cartModel.getCreate_at());
        contentValues.put(KEY_STATUS, cartModel.getStatus());

        database.insert(TABLE_CART, null, contentValues);
        database.close();
        return true;
    }

    public boolean updateFood(String id_food, int newQty, String date) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_QUANTITY, newQty);
        contentValues.put(KEY_DATE_FOOD, date);
        sqLiteDatabase.update(TABLE_FOOD, contentValues, KEY_FOOD_ID + "= ?", new String[]{id_food});
        return true;
    }

    public void updateFoodQuantity(String id_food, String qty, String date) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_QUANTITY, qty);
        contentValues.put(KEY_DATE_FOOD, date);
        sqLiteDatabase.update(TABLE_FOOD, contentValues, KEY_FOOD_ID + "= ?", new String[]{id_food});
    }

    public boolean removeMyCart(String id) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("delete from " + TABLE_CART + " where " + id + "=" + KEY_CART_ID);
        database.close();
        getCart();
        return true;
    }

    public boolean AddNewCart(CartModel cartModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CART_ID, cartModel.getId());
        contentValues.put(KEY_USER_ID, cartModel.get_idUser());
        contentValues.put(KEY_FOOD_ID, cartModel.get_idFood());
        contentValues.put(KEY_NAME_FOOD, cartModel.getNameFood());
        contentValues.put(KEY_IMAGE_FOOD, cartModel.getImageFood());
        contentValues.put(KEY_COUNT, cartModel.getCount());
        contentValues.put(KEY_TOTAL_PRICE, cartModel.getTotalPrice());
        contentValues.put(KEY_NOTE, cartModel.getNote());
        contentValues.put(KEY_CREATE_AT, cartModel.getCreate_at());
        contentValues.put(KEY_STATUS, cartModel.getStatus());

        database.insert(TABLE_CART, null, contentValues);
        database.close();
        return true;
    }

    public boolean addHistory(HistoriModel historiModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_HISTORY_ID, historiModel.getId());
        contentValues.put(KEY_NAME_USER, historiModel.getNamaLengkap());
        contentValues.put(KEY_NAME_FOOD_HISTORY, historiModel.getNamaMakanan());
        contentValues.put(KEY_ROOM_NUMBER_HISTORY, historiModel.getNomorKamar());
        contentValues.put(KEY_COUNT_HISTORY, historiModel.getJumlah());
        contentValues.put(KEY_TOTAL_HISTORY, historiModel.getTotal());
        contentValues.put(KEY_DATE, historiModel.getTanggal());

        database.insert(TABLE_HISTORY, null, contentValues);
        database.close();
        return true;
    }
}
