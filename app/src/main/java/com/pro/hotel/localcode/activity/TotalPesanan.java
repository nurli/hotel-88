package com.pro.hotel.localcode.activity;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.pro.hotel.R;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.CartModel;
import com.pro.hotel.localcode.model.HistoriModel;
import com.pro.hotel.localcode.utils.Config;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TotalPesanan extends AppCompatActivity {

    TextInputLayout tilNamaLengkap, tilNomorKamar, tilNomorHandphone, tilEstimasiSampai;
    EditText editTextNamaLengkap, editTextNomorKamar, editTextNomorHandphone, editTextEstimasiSampai;
    Spinner spinnerLokasi, spinnerMetodeBayar;
    ImageView imageViewFood, imageViewTime, imageViewBack;
    Button buttonPesan, buttonLess, buttonAdd;
    TextView textViewJumlah, textViewTotal;

    int count = 1, q, sum;
    int harga;
    String id, user, phone, id_food, nameFood, _lokasi, _metode, strTime, roomNumber;
    SQLiteHelper sqLiteHelper;
    Bitmap bitmap;
    private TimePickerDialog timePickerDialog;
    private SimpleDateFormat simpleDateFormat;


    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_pesanan);

        init();

        sqLiteHelper = new SQLiteHelper(this);

        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        id = sharedPreferences.getString(Config.ID_USER, "Not Available");
        user = sharedPreferences.getString(Config.USERNAME, "Not Available");
        phone = sharedPreferences.getString(Config.PHONE_NUMBER, "Not Available");
        roomNumber = sharedPreferences.getString(Config.ROOM_NUMBER, "Not Available");

        editTextNomorHandphone.setText(phone);
        editTextNomorKamar.setText(roomNumber);
        editTextNamaLengkap.setText(user);


        Bundle bundle = getIntent().getExtras();
        byte[] bytes = bundle.getByteArray("image_food");
        bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        imageViewFood.setImageBitmap(bitmap);

        final Intent intent = getIntent();
        harga = Integer.parseInt(intent.getStringExtra("harga"));
        id_food = intent.getStringExtra("id");
        nameFood = intent.getStringExtra("name_food");

        textViewTotal.setText(String.valueOf(harga));

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final String[] lokasi = new String[]{
                "Di Hotel", "Di Luar Hotel"
        };

        List<String> list = new ArrayList<>(Arrays.asList(lokasi));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, list
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLokasi.setAdapter(adapter);
        _lokasi = String.valueOf(spinnerLokasi.getSelectedItem());

        final String[] metodeBayar = new String[]{
                "Tunai Lobi", "Total CheckOut"
        };

        List<String> list1 = new ArrayList<>(Arrays.asList(metodeBayar));
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, list1
        );
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMetodeBayar.setAdapter(adapter1);
        _metode = String.valueOf(spinnerMetodeBayar.getSelectedItem());

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 10) {
                    count = 10;
                    textViewJumlah.setText(String.valueOf(count));
                } else {
                    count = count + 1;
                    textViewJumlah.setText(String.valueOf(count));
                    sum = count * harga;
                    textViewTotal.setText(String.valueOf(sum));
                        /*if (q < count) {
                            editTextNote.setVisibility(View.GONE);
                            buttonAddCart.setVisibility(View.GONE);
                            count = q;
                            textViewCount.setText(String.valueOf(count));
                            sum = count * Integer.parseInt(textViewSellingPrice.getText().toString());
                            textViewSumary.setText(String.valueOf(sum));
                            Toast.makeText(OrderFood.this, "This item has " + q + " servings", Toast.LENGTH_SHORT).show();*/
                        /*} else {
                            if (count >= 0) {
                                editTextNote.setVisibility(View.VISIBLE);
                                buttonAddCart.setVisibility(View.VISIBLE);
                            } else {
                                editTextNote.setVisibility(View.GONE);
                                buttonAddCart.setVisibility(View.GONE);
                            }*/
//                    Log.d("Count ", String.valueOf(count));
                }
                /*if (textViewJumlah.getText().toString().equals("Available")) {
                }*/
                /*} else if (textViewStatusQuantity.getText().toString().equals("Not Available")) {
                    Toast.makeText(OrderFood.this, "Your Order is Not Available", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        buttonLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 1) {
                    count = 1;
                    textViewJumlah.setText(String.valueOf(count));
                    /*editTextNote.setVisibility(View.GONE);
                    buttonAddCart.setVisibility(View.GONE);*/
                } else {
                    count = count - 1;
                    textViewJumlah.setText(String.valueOf(count));
                    sum = count * harga;
                    textViewTotal.setText(String.valueOf(sum));
                    /*if (count == 0) {
                        editTextNote.setVisibility(View.GONE);
                        buttonAddCart.setVisibility(View.GONE);
                    } else {
                        editTextNote.setVisibility(View.VISIBLE);
                        buttonAddCart.setVisibility(View.VISIBLE);
                    }*/
                    Log.d("Count ", String.valueOf(count));

                }
            }
        });
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        imageViewTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog = new TimePickerDialog(TotalPesanan.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        editTextEstimasiSampai.setText(hourOfDay + ":" + minute);
                    }
                }, 0, 0, true);
                timePickerDialog.show();
            }
        });

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        final byte imageFood[] = byteArrayOutputStream.toByteArray();

        final int _count = Integer.parseInt(textViewJumlah.getText().toString());
//        final int _total = Integer.parseInt(textViewTotal.getText().toString());
        final int _total = harga * _count;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        final String _strTime = simple.format(calendar.getTime());
        final String date = _strTime;

        buttonPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent1 = new Intent(TotalPesanan.this, TambahPesanan.class);
//                intent1.putExtra("name_food", )
//                startActivity(intent1);
                if (validate()) {
                    boolean isAdd = sqLiteHelper.AddNewCart(new CartModel(null,
                            id, id_food, nameFood, imageFood, _count, _total, strTime, date, _metode));
                    boolean isAdds = sqLiteHelper.addHistory(new HistoriModel(null,
                            editTextNamaLengkap.getText().toString(),
                            nameFood,
                            editTextNomorKamar.getText().toString(),
                            textViewJumlah.getText().toString(),
                            textViewTotal.getText().toString(),
                            date));
                    if (isAdd == true && isAdds == true) {
                        Toast.makeText(TotalPesanan.this, "Success", Toast.LENGTH_SHORT).show();
                        count = 0;
                        EmptyField();
                        onBackPressed();
                    } else {
                        Toast.makeText(TotalPesanan.this, "Failed!!!", Toast.LENGTH_SHORT).show();
                    }
                }
                /*
                boolean isAdd = sqLiteHelper.addCart(new CartModel(null,
                        id, id_food, nameFood, imageFood, _count, _total, _lokasi, strTime, _metode
                        ));
                if (isAdd == true) {
                    Toast.makeText(TotalPesanan.this, "Success", Toast.LENGTH_SHORT).show();
                    count = 0;
                    EmptyField();
                    onBackPressed();
                }else {
                    Toast.makeText(TotalPesanan.this, "Failed!!!", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    private void EmptyField() {
        editTextNamaLengkap.setText("");
        editTextNomorKamar.setText("");
        editTextNomorHandphone.setText("");
        editTextEstimasiSampai.setText("");
    }

    private boolean validate() {
        boolean valid = false;
        //Get values from EditText fields
        String namaLengkap = editTextNamaLengkap.getText().toString();
        String nomorKamar = editTextNomorKamar.getText().toString();
        String nomorHp = editTextNomorHandphone.getText().toString();
        String estimasiSampai = editTextEstimasiSampai.getText().toString();

        //Handling validation for First Name field
        if (namaLengkap.isEmpty()) {
            valid = false;
            tilNamaLengkap.setError("Please enter your Name");
        } else {
            valid = true;
            tilNamaLengkap.setError(null);
        }

        //Handling validation for First Name field
        if (nomorKamar.isEmpty()) {
            valid = false;
            tilNomorKamar.setError("Please enter your Room Number");
        } else {
            if (nomorKamar.length() == 3) {
                valid = true;
                tilNomorKamar.setError(null);
            } else {
                valid = false;
                tilNomorKamar.setError("Your Room Number is to short!");
            }
        }

        //Handling validation for Phone Number Field
        if (nomorHp.isEmpty()) {
            valid = false;
            tilNomorHandphone.setError("Please enter Your Contact");
        } else {
            valid = true;
            tilNomorHandphone.setError(null);
        }
        //Handling validation for Email Field
        if (estimasiSampai.isEmpty()) {
            valid = false;
            tilEstimasiSampai.setError("Please enter Your Estimation");
        } else {
            valid = true;
            tilEstimasiSampai.setError(null);
        }
        return valid;
    }

    private void init() {
        textViewTotal = findViewById(R.id.textViewTotal);
        tilNamaLengkap = findViewById(R.id.tilNamaLengkap);
        tilNomorKamar = findViewById(R.id.tilNomorKamar);
        tilNomorHandphone = findViewById(R.id.tilNomorHandphone);
        tilEstimasiSampai = findViewById(R.id.tilEstimasiSampai);
        editTextNamaLengkap = findViewById(R.id.editTextNamaLengkap);
        editTextNomorKamar = findViewById(R.id.editTextNoKamar);
        editTextNomorHandphone = findViewById(R.id.editTextNomorHandphone);
        editTextEstimasiSampai = findViewById(R.id.editTextEstimasiSampai);
        spinnerLokasi = findViewById(R.id.spinnerLokasi);
        spinnerMetodeBayar = findViewById(R.id.spinnerMetodePembayaran);
        imageViewFood = findViewById(R.id.imageViewFood);
        imageViewTime = findViewById(R.id.imageViewTime);
        imageViewBack = findViewById(R.id.imageViewBack);
        buttonPesan = findViewById(R.id.buttonPesan);
        buttonLess = findViewById(R.id.buttonLess);
        buttonAdd = findViewById(R.id.buttonAdd);
        textViewJumlah = findViewById(R.id.textViewCount);
    }
}
