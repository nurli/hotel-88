package com.pro.hotel.localcode.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pro.hotel.R;
import com.pro.hotel.localcode.adapter.CartAdapter;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.CartModel;

import java.util.ArrayList;
import java.util.List;

public class MyCart extends AppCompatActivity {

    FloatingActionButton floatingActionButtonAdd;
    ListView listViewCart;
    CartModel cartModel;
    EditText editTextSearch;
    List<CartModel> cartModelList;
    CartAdapter cartAdapter;
    SQLiteHelper sqLiteHelper;
    SwipeRefreshLayout swipeRefreshLayout;
    String _id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);

        init();

        cartModelList = new ArrayList<>();
        cartModel = new CartModel();
        _id = cartModel.getId();

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        sqLiteHelper = new SQLiteHelper(this);

        showMyCart();

        floatingActionButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyCart.this, BuatPesanan.class);
                startActivity(intent);
            }
        });

        /*swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                cartModelList = new ArrayList<>(sqLiteHelper.getCart());
                cartAdapter = new CartAdapter(MyCart.this, (ArrayList<CartModel>) cartModelList);
                listViewCart.setAdapter(cartAdapter);
            }
        });*/
        listViewCart.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                cartModel.getId();
                AlertDialog.Builder alert = new AlertDialog.Builder(MyCart.this);
                alert.setTitle("Delete");
                alert.setMessage("Are you sure you want to Delete this Item?")
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        boolean isDelete = sqLiteHelper.removeMyCart(String.valueOf(position));
                                        if (isDelete == true) {
                                            Toast.makeText(MyCart.this, "Delete Item Success", Toast.LENGTH_SHORT).show();
                                            showMyCart();
                                        }else {
                                            Toast.makeText(MyCart.this, "Delete Item is Failed!!!", Toast.LENGTH_SHORT).show();
                                        }
//                                        sqLiteHelper.getCart();
//                                        refresh();
//                                        cartAdapter.notifyDataSetChanged();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
                return false;
            }
        });
    }

    public void refresh(){
        cartModelList = new ArrayList<>(sqLiteHelper.getCart());
        cartAdapter = new CartAdapter(MyCart.this, (ArrayList<CartModel>) cartModelList);
        listViewCart.setAdapter(cartAdapter);
    }

    public void showMyCart() {
        final ArrayList<CartModel> cartModels = new ArrayList<>(sqLiteHelper.getCart());
        cartAdapter = new CartAdapter(this, cartModels);
        listViewCart.setAdapter(cartAdapter);
    }

    private void filter(String name) {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        List<CartModel> cartModelList = sqLiteHelper.searchMyCart(name);
        if (cartModelList!= null) {
            cartAdapter = new CartAdapter(MyCart.this, (ArrayList<CartModel>) cartModelList);
            listViewCart.setAdapter(cartAdapter);
        }
    }

    private void init() {
        floatingActionButtonAdd = findViewById(R.id.fabAdd);
        listViewCart = findViewById(R.id.listViewCart);
        editTextSearch = findViewById(R.id.editTextSearch);
        swipeRefreshLayout = findViewById(R.id.swipe);
    }
}
