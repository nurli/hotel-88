package com.pro.hotel.localcode.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.pro.hotel.R;
import com.pro.hotel.localcode.database.SQLiteHelper;
import com.pro.hotel.localcode.model.UserModel;
import com.pro.hotel.localcode.utils.Config;

public class Login extends AppCompatActivity {

    TextInputLayout tilEmail, tilPassword;
    EditText editTextEmail, editTextPassword;
    Button buttonLogin;
    TextView textViewForgotPassword, textViewSignUp;

    SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        sqLiteHelper = new SQLiteHelper(this);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (sqLiteHelper.isUserExists(editTextEmail.getText().toString())) {
                        String email = editTextEmail.getText().toString();
                        String password = editTextPassword.getText().toString();
                        UserModel userModel = sqLiteHelper.Authenticate(new UserModel(null, email, password));
                        if (userModel != null && userModel.getLevel().equals("1")) {
                            Toast.makeText(Login.this, "Log In Success", Toast.LENGTH_SHORT).show();
                            SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Config.ID_USER, userModel.getId());
                            editor.putString(Config.USERNAME, userModel.getUsername());
                            editor.putString(Config.PHONE_NUMBER, userModel.getPhoneNumber());
                            editor.putString(Config.LEVEL_USER, userModel.getLevel());
                            editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, true);
                            editor.commit();
                            Intent intent = new Intent(Login.this, MainAdmin.class);
                            startActivity(intent);
                            finish();
                        } else if (userModel != null && userModel.getLevel().equals("2")) {
                            Toast.makeText(Login.this, "Log In Success", Toast.LENGTH_SHORT).show();
                            SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Config.ID_USER, userModel.getId());
                            editor.putString(Config.USERNAME, userModel.getUsername());
                            editor.putString(Config.PHONE_NUMBER, userModel.getPhoneNumber());
                            editor.putString(Config.LEVEL_USER, userModel.getLevel());
                            editor.putString(Config.ROOM_NUMBER, userModel.getNoKamar());
                            editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, true);
                            editor.commit();
                            Intent intent = new Intent(Login.this, MainUser.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(Login.this, "Log In Failed!, Please Try Again", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Login.this, "Your Email is not Sign In !", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Login.this, SignUp.class);
                        startActivity(intent);
                    }
                }
            }
        });

        textViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, SignUp.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Boolean loggedin = sharedPreferences.getBoolean(Config.LOGGEDIN_SHARED_PREF, false);
        String level = sharedPreferences.getString(Config.LEVEL_USER, "Not Available");
        if (loggedin == true && level.equals("1")) {
            Intent intent = new Intent(Login.this, MainAdmin.class);
            startActivity(intent);
            finish();
        } else if (loggedin == true && level.equals("2")) {
            Intent intent = new Intent(Login.this, MainUser.class);
            startActivity(intent);
            finish();
        } else if (loggedin == false) {

        }
    }

    private boolean validate() {
        boolean valid = false;
        //Get values from EditText fields
//        String first = editTextUsername.getText().toString();
//        String phone = editTextPhoneNumber.getText().toString();
        String email = editTextEmail.getText().toString();
        String pass = editTextPassword.getText().toString();
//        String repass = editTextRePassword.getText().toString();

        //Handling validation for First Name field
        /*if (first.isEmpty()) {
            valid = false;
            tilUsename.setError("Please enter Your Name");
        } else {
            if (first.length() >= 4) {
                valid = true;
                tilUsename.setError(null);
            } else {
                valid = false;
                tilUsename.setError("Your First Name is to short!");
            }
        }

        //Handling validation for Phone Number Field
        if (phone.isEmpty()) {
            valid = false;
            tilPhoneNumber.setError("Please enter Your Phone Number");
        } else {
            if (phone.length() > 8) {
                valid = true;
                tilPhoneNumber.setError(null);
            } else {
                valid = false;
                tilPhoneNumber.setError("Your Phone Number is to short!");
            }
        }*/
        //Handling validation for Email Field
        if (email.isEmpty()) {
            valid = false;
            tilEmail.setError("Please enter Your Name");
        } else {
            //Handling validation for Email field
            valid = true;
            tilEmail.setError(null);
//            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//                valid = false;
//                tilEmail.setError("Please enter valid Email!");
//            } else {
//            }
        }

        //Handling validation for Password field
        if (pass.isEmpty()) {
            valid = false;
            tilPassword.setError("Please Enter Valid Room Number");
        } else {
            if (pass.length() <= 5) {
                valid = true;
                tilPassword.setError(null);
            } else {
                valid = false;
                tilPassword.setError("Room Number is to short!");
            }
        }
        //Handling validation for Re-Password field
        /*if (repass.isEmpty()) {
            valid = false;
            tilRePassword.setError("Please Enter Valid Re-Password");
        } else {
            if (repass.length() > 5) {
                valid = true;
                tilRePassword.setError(null);
            } else {
                valid = false;
                tilRePassword.setError("Password is to short!");
            }
        }
        if (!pass.equals(repass)) {
            valid = false;
            tilRePassword.setError("Verify your passwords with a password Not Equal!");
        }*/
        return valid;
    }

    private void init() {
        tilEmail = findViewById(R.id.tilEmail);
        tilPassword = findViewById(R.id.tilPassword);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonLogin = findViewById(R.id.buttonLogIn);
        textViewForgotPassword = findViewById(R.id.textViewForgotPassword);
        textViewSignUp = findViewById(R.id.textViewRegister);
    }
}
