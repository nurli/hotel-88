package com.pro.hotel.localcode.model;

public class FoodModel {

    public String id;
    public String foodName;
    public String quantity;
    public String purchasePrice;
    public String sellingPrice;
    public byte[] imageFood;
    public String date;
    public String description;

    public FoodModel(){

    }

    public FoodModel(String id, String foodName, String quantity, String purchasePrice, String sellingPrice, byte[] imageFood, String date, String description){
        this.id = id;
        this.foodName = foodName;
        this.quantity = quantity;
        this.purchasePrice = purchasePrice;
        this.sellingPrice = sellingPrice;
        this.imageFood = imageFood;
        this.date = date;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public byte[] getImageFood() {
        return imageFood;
    }

    public void setImageFood(byte[] imageFood) {
        this.imageFood = imageFood;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
