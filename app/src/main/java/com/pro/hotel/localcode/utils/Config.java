package com.pro.hotel.localcode.utils;

public class Config {
    public static final String SHARED_PREF_NAME = "myloginapp";

    public static final String LOGGEDIN_SHARED_PREF = "loggedin";

    public static final String LEVEL_USER = "level";
    public static final String EMAIL_USER = "email";
    public static final String ID_USER = "id_user";
    public static final String USERNAME = "username";
    public static final String PHONE_NUMBER = "phonenumber";
    public static final String ROOM_NUMBER = "roomnumber";
}
